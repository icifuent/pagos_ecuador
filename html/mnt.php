<?php

Flight::set('PAIS_ID', 1);
Flight::set('MNT_MODU_ID', 2);
Flight::set('PERI_ID_A_SOLICITUD',14);
Flight::set('FORM_INGRESO',1);
Flight::set('FORM_SALIDA',4);
//____________________________________________________________
//Helpers
Flight::map('ObtenerDetalleMantenimiento', function($db,$cont_id,$mant_id){
    $query = "  SELECT
                    mnt.*,
                    emp.*,
                    empcl.clas_nombre,
                    mp.*,
                    pe.peri_id,
                    pe.peri_nombre,
                    e.empr_nombre,
                    esp.espe_nombre,
                    co.comu_nombre,
                    GROUP_CONCAT(t.tecn_nombre) as tecn_nombre,
                    z.zona_id, z.zona_nombre,
                    r.regi_id, r.regi_nombre
                FROM
                    mantenimiento mnt,
                    empresa e,
                    rel_contrato_empresa rce,
                    especialidad esp,
                    emplazamiento emp,
                    emplazamiento_clasificacion empcl,
                    comuna co,
                    region r,
                    provincia p,
                    tecnologia t,
                    rel_emplazamiento_tecnologia ret,
                    zona z,
                    rel_zona_emplazamiento rze,
                    periodicidad pe,
                    rel_contrato_periodicidad rcp,
                    mantenimiento_periodos mp
                WHERE
                    mnt.mant_id= ".$mant_id."

                    AND mnt.cont_id = ".$cont_id."
                    AND rce.cont_id=mnt.cont_id
                    AND rce.empr_id=e.empr_id
                    AND mnt.empr_id=e.empr_id

                    AND mnt.empl_id=emp.empl_id
                    AND mnt.espe_id=esp.espe_id

                    AND mnt.clas_id = empcl.clas_id
                    AND emp.comu_id=co.comu_id
                    AND p.prov_id = co.prov_id
                    AND r.regi_id = p.regi_id
                    AND ret.empl_id=emp.empl_id
                    AND ret.tecn_id=t.tecn_id
                    AND rze.empl_id = emp.empl_id
                    AND rze.zona_id = z.zona_id
                    AND z.cont_id = mnt.cont_id
                    AND z.zona_tipo = 'CONTRATO'
                    AND mnt.mape_id = mp.mape_id
                    AND mp.rcpe_id = rcp.rcpe_id
                    AND rcp.peri_id = pe.peri_id
                    AND rcp.cont_id = mnt.cont_id
                GROUP BY
                    t.tecn_nombre
                ;";
    $res =  $db->ExecuteQuery($query);
    if ($res['status'] == 0){
        return $res;
    }

    if(0 < $res['rows']){
        //archivos relacionados
        $res_repo = $db->ExecuteQuery("SELECT * FROM repositorio r WHERE r.repo_tabla_id = $mant_id AND r.repo_tabla = 'mantenimiento';");
        if ($res_repo['status'] == 0){
            return $res_repo;
        }
        $res['data'][0]['archivos'] = $res_repo['data'];


        //responsables
        $res_resp = $db->ExecuteQuery("SELECT DISTINCT
                                       usuario.usua_id,
                                       usuario.usua_nombre,
                                       perfil.perf_nombre
                                     FROM
                                       mantenimiento
                                     INNER JOIN rel_zona_emplazamiento ON (rel_zona_emplazamiento.empl_id=mantenimiento.empl_id)
                                     INNER JOIN rel_contrato_usuario_responsabilidad ON (rel_contrato_usuario_responsabilidad.zona_id=rel_zona_emplazamiento.zona_id)
                                     INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.recu_id=rel_contrato_usuario_responsabilidad.recu_id)
                                     INNER JOIN usuario ON (usuario.usua_id=rel_contrato_usuario.usua_id AND usuario.usua_estado='ACTIVO')
                                     INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                                     INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND (perfil.perf_nombre='GESTOR' OR (perfil.perf_nombre='DESPACHADOR' AND usuario.empr_id=mantenimiento.empr_id)) )
                                     WHERE
                                         mant_id=$mant_id");

        if ($res_resp['status'] == 0){
            return $res_resp;
        }
        $res['data'][0]['gestor_responsable'] = array();
        $res['data'][0]['despachador_responsable'] = array();
        foreach($res_resp['data'] AS $row){
            if($row['perf_nombre'] == "GESTOR"){
                array_push($res['data'][0]['gestor_responsable'],$row['usua_nombre']);
            }
            else{
                array_push($res['data'][0]['despachador_responsable'],$row['usua_nombre']);
            }
        }
    }
    return $res;
});

//____________________________________________________________
//Bandeja
Flight::route('GET /contrato/@id:[0-9]+/mnt/bandeja/filtros', function($id){
    //session_start();

    $pais_id = Flight::get('PAIS_ID');
    $modu_id = Flight::get('MNT_MODU_ID');

    $usua_id = $_SESSION['user_id'];
    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['clusters'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='SUBGERENCIA'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['sub_gerencias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=$pais_id ORDER BY regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT
                                    e.espe_id, e.espe_nombre
                                FROM especialidad e
                                INNER JOIN rel_modulo_especialidad reme ON(reme.espe_id = e.espe_id AND reme.modu_id=$modu_id)
                                WHERE e.espe_estado='ACTIVO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['especialidades'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT p.peri_id, p.peri_nombre FROM periodicidad p, rel_contrato_periodicidad rcp WHERE rcp.cont_id=".$id." AND rcp.peri_id=p.peri_id");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['periodos'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT * FROM emplazamiento_clasificacion");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['clasificaciones'] = $res['data'];

    $res = $dbo->ExecuteQuery( "SHOW COLUMNS FROM mantenimiento WHERE Field = 'mant_estado'" );
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    preg_match("/^enum\(\'(.*)\'\)$/", $res['data'][0]['Type'], $matches);
    $out['estados'] = explode("','", $matches[1]);
    //$out['estados'] = array( 'CREADA','ASIGNANDO','ASIGNADA','EJECUTANDO','VALIDANDO','FINALIZADA' );

    //Listado de tecnicos asignados, limitado a la empresa del usuario si no es cliente
    $res = $dbo->ExecuteQuery("SELECT rce.coem_tipo FROM usuario u, rel_contrato_empresa rce WHERE u.usua_id=$usua_id AND rce.empr_id=u.empr_id AND rce.cont_id=$id");
    if ($res['status'] == 0 || count($res['data'])==0 ) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    if( $res['data'][0]['coem_tipo']!='CLIENTE' )
        {
        $res = $dbo->ExecuteQuery( "SELECT u.usua_id, u.usua_nombre FROM
                                            contrato c
                                            , usuario u
                                            , modulo m
                                            , rel_contrato_usuario rcu
                                            , rel_contrato_usuario_modulo rcum

                                            , usuario u_actual
                                            , rel_contrato_usuario rcu_actual
                                            , empresa e
                                    WHERE
                                            c.cont_id = $id

                                            AND u_actual.usua_id = $usua_id
                                            AND rcu_actual.cont_id = c.cont_id
                                            AND rcu_actual.usua_id = u_actual.usua_id
                                            AND u_actual.empr_id = e.empr_id
                                            AND u.empr_id = e.empr_id

                                            AND rcu.cont_id = c.cont_id
                                            AND rcu.usua_id = u.usua_id

                                            AND rcum.recu_id = rcu.recu_id
                                            AND rcum.modu_id = m.modu_id

                                            AND m.modu_alias = 'MNT'
                                            AND u.usua_cargo = 'JEFE_CUADRILLA'
                                    ;");

        if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
        $out['tecnicos'] = $res['data'];
    }
    else{
        $res = $dbo->ExecuteQuery( "SELECT u.usua_id, u.usua_nombre FROM
                                            contrato c
                                            , usuario u
                                            , modulo m
                                            , rel_contrato_usuario rcu
                                            , rel_contrato_usuario_modulo rcum
                                    WHERE
                                            c.cont_id = $id
                                            AND rcu.cont_id = c.cont_id
                                            AND rcu.usua_id = u.usua_id
                                            AND rcum.recu_id = rcu.recu_id
                                            AND rcum.modu_id = m.modu_id
                                            AND m.modu_alias = 'MNT'
                                            AND u.usua_cargo = 'JEFE_CUADRILLA'
                                    ;");

        if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
        $out['tecnicos'] = $res['data'];
    }

     $res = $dbo->ExecuteQuery("SELECT usuario.usua_id, UPPER(usua_nombre) AS usua_nombre
                              FROM mantenimiento
                              INNER JOIN usuario ON (mantenimiento.usua_creador = usuario.usua_id)
                              WHERE cont_id=$id
                              GROUP BY usuario.usua_id
                              ORDER BY usua_nombre");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['usuarios_creador'] = $res['data'];


    Flight::json($out);
});

Flight::route('GET|POST /contrato/@id:[0-9]+/mnt/list(/@page:[0-9]+)', function($id,$page){

    //session_start();
    $usua_id = $_SESSION['user_id'];

    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = "";
    $filtros_zonas_contrato_flag = FALSE;
    $filtros_zonas_cluster_flag = FALSE;
    $filtros_zonas_subgerencia_flag = FALSE;
    $filtros_periodos_flag = FALSE;
    $filtros_tecnico_flag = FALSE;
    $out = array();
    $out['pagina'] = $page;
    $out['filtros'] = $filtros_ini;

    foreach ($filtros_ini as $key => $value) {
        if( is_null($value) ){
            unset($filtros_ini[$key]);
        }
        if( !is_array($value) && strlen(trim($value.""))==0 ){
            unset($filtros_ini[$key]);
        }
    }

    //Sacamos los filtros especiales del arreglo de filtros
    if( isset($filtros_ini['espe_id']) ){
        $filtros .= " AND espe.espe_id=".$filtros_ini['espe_id']." ";
        unset( $filtros_ini['espe_id'] );
    }

    if( isset($filtros_ini['clas_id']) ){
        $filtros .= " AND mnt.clas_id=".$filtros_ini['clas_id']." ";
        unset( $filtros_ini['clas_id'] );
    }

    if( isset($filtros_ini['zona_id']) ){
        $filtros .= " AND rzeco.zona_id=".$filtros_ini['zona_id']." AND rzeco.empl_id=empl.empl_id AND rzeco.zona_id=zco.zona_id AND  zco.zona_tipo='CONTRATO' ";
        unset( $filtros_ini['zona_id'] );
        $filtros_zonas_contrato_flag = TRUE;
    }

    if( isset($filtros_ini['clus_id']) ){
        $filtros .= " AND rzecl.zona_id=".$filtros_ini['clus_id']." AND rzecl.empl_id=empl.empl_id AND rzecl.zona_id=zcl.zona_id AND  zcl.zona_tipo='CLUSTER' ";
        unset( $filtros_ini['clus_id'] );
        $filtros_zonas_cluster_flag = TRUE;
    }

    if( isset($filtros_ini['subg_id']) ){
        $filtros .= " AND rzesg.zona_id=".$filtros_ini['subg_id']." AND rzesg.empl_id=empl.empl_id AND rzesg.zona_id=zsg.zona_id AND  zsg.zona_tipo='SUBGERENCIA' ";
        unset( $filtros_ini['subg_id'] );
        $filtros_zonas_subgerencia_flag = TRUE;
    }

    if( isset($filtros_ini['mant_fecha_programada_inicio']) ){
        $filtros .= " AND mnt.mant_fecha_programada >= '".$filtros_ini['mant_fecha_programada_inicio']." 00:00:00' "    ;
        unset($filtros_ini['mant_fecha_programada_inicio']);
    }

    if( isset($filtros_ini['mant_fecha_programada_termino']) ){
        $filtros .= " AND mnt.mant_fecha_programada <= '".$filtros_ini['mant_fecha_programada_termino']." 23:59:59' "    ;
        unset($filtros_ini['mant_fecha_programada_termino']);
    }

    if( isset($filtros_ini['regi_id']) ){
        $filtros .= " AND r.regi_id=".$filtros_ini['regi_id']." ";
        unset( $filtros_ini['regi_id'] );
    }

    if( isset($filtros_ini['peri_id']) ){
        $filtros .= " AND pe.peri_id=".$filtros_ini['peri_id']." AND rcp.peri_id = pe.peri_id AND rcp.cont_id = mnt.cont_id AND mp.rcpe_id = rcp.rcpe_id AND mnt.mape_id = mp.mape_id";
        unset( $filtros_ini['peri_id'] );
        $filtros_periodos_flag = TRUE;
    }
    else{
        $filtros .= " AND rcp.peri_id = pe.peri_id AND rcp.cont_id = mnt.cont_id AND mp.rcpe_id = rcp.rcpe_id AND mnt.mape_id = mp.mape_id";
    }

    if( isset($filtros_ini['mant_estado']) ){
        if(is_array($filtros_ini['mant_estado'])){
            $estados = array();
            foreach($filtros_ini['mant_estado'] as $a){
                array_push($estados,"'".$a."'");
            }
            $estados = implode(",",$estados);
        }
        else{
            $estados = "'".$filtros_ini['mant_estado']."'";
        }

        $filtros .= " AND mnt.mant_estado IN (".$estados.")";
        unset( $filtros_ini['mant_estado'] );
    }
    
    if( isset($filtros_ini['mant_responsable']) && $filtros_ini['mant_responsable']!="" ){
        $filtros .= " AND mnt.mant_responsable = '".$filtros_ini['mant_responsable']."' ";
    }

    //tecnico
    if( isset($filtros_ini['usua_id']) ){
        $superquery = " SELECT SQL_CALC_FOUND_ROWS * FROM (
                            %s
                        ) t1
                        INNER JOIN
                        (
                            SELECT * FROM (
                                    SELECT * FROM (
                                            SELECT
                                                    mnt.mant_id,
                                                    ma.maas_id,
                                                    u.usua_id,
                                                    u.usua_nombre
                                            FROM
                                                    mantenimiento mnt
                                                    , mantenimiento_asignacion ma
                                                    , rel_mantenimiento_asignacion_usuario rmau
                                                    , usuario u
                                            WHERE
                                                    ma.mant_id = mnt.mant_id
                                                    AND ma.maas_id = rmau.maas_id
                                                    AND rmau.usua_id = u.usua_id
                                                    AND rmau.rmau_tipo = 'JEFECUADRILLA'
                                                    #AND u.usua_estado = 'ACTIVO'
                                            ORDER BY
                                                    ma.maas_id DESC
                                            ) t
                                    GROUP BY mant_id
                                    ) tt
                            WHERE usua_id = ".$filtros_ini['usua_id']."
                        ) t2
                        ON t1.mant_id=t2.mant_id";
        unset( $filtros_ini['usua_id'] );
        $filtros_tecnico_flag = TRUE;
    }

    if( isset($filtros_ini['usua_creador']) ){
        $filtros .= " AND mnt.usua_creador=".$filtros_ini['usua_creador']." ";
        unset( $filtros_ini['usua_creador'] );
    }

    //Obtenemos el resto de filtros
    $filtros = Flight::filtersToWhereString( array("emplazamiento"), $filtros_ini).$filtros;

    $query = "SELECT
                 SQL_CALC_FOUND_ROWS
                 mnt.*,
                 empl.empl_nombre,
                 empl.empl_nemonico,
                 empl.empl_direccion,
                 pe.peri_nombre,
                 espe.espe_nombre,
                 empl_clas.clas_nombre,
                 CONCAT(mp.mape_fecha_inicio,' 00:00:00') AS mape_fecha_inicio,
                 CONCAT(mp.mape_fecha_cierre,' 00:00:00') AS mape_fecha_cierre "
            . "FROM "
                . "mantenimiento mnt "
                . ", emplazamiento empl "
                . ", emplazamiento_clasificacion empl_clas "
                . ", especialidad espe "
                . ", comuna co "
                . ", region r "
                . ", provincia p "
                . ", periodicidad pe, rel_contrato_periodicidad rcp, mantenimiento_periodos mp "
                . ", usuario u, rel_contrato_usuario rcu "
                . ", rel_contrato_usuario_alcance rcua , rel_zona_emplazamiento rzeal, zona zal "
                .(($filtros_zonas_contrato_flag)?", rel_zona_emplazamiento rzeco, zona zco ":"" )
                .(($filtros_zonas_cluster_flag)?", rel_zona_emplazamiento rzecl, zona zcl ":"" )
                .(($filtros_zonas_subgerencia_flag)?", rel_zona_emplazamiento rzesg, zona zsg ":"" )

            . "WHERE "
                . "mnt.cont_id = ".$id." "
                //. "AND mnt.mant_estado!='FINALIZADA' "
                . "AND mnt.empl_id=empl.empl_id "
                . "AND empl_clas.clas_id=mnt.clas_id "
                . "AND mnt.espe_id=espe.espe_id "
                . "AND empl.comu_id=co.comu_id "
                . "AND p.prov_id = co.prov_id "
                . "AND r.regi_id = p.regi_id "

                . "AND rzeal.empl_id=empl.empl_id "
                . "AND rzeal.zona_id=zal.zona_id "
                . "AND zal.zona_tipo='ALCANCE' "

                . "AND u.usua_id = $usua_id "
                . "AND rcu.usua_id = u.usua_id "
                . "AND rcu.cont_id = mnt.cont_id "
                . "AND rcua.recu_id = rcu.recu_id "
                . "AND rcua.zona_id = rzeal.zona_id "

                . "AND mnt.mape_id = mp.mape_id "
                . "AND mp.rcpe_id = rcp.rcpe_id "
                . "AND rcp.peri_id = pe.peri_id "
                . "AND rcp.cont_id = mnt.cont_id "

                . "AND ".$filtros
            . ((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;

    if( $filtros_tecnico_flag ){
        $query = sprintf( $superquery, str_replace("SQL_CALC_FOUND_ROWS","",$query) );
    }

    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    $out['mantenimientos'] = $res['data'];

    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $out['total'] = intval($res_count['data'][0]['total']);
    $out['paginas'] = ceil($out['total']/$results_by_page);
    $out['status'] = 1;


    foreach($out['mantenimientos'] as &$mantenimiento) {
        $mant_id = $mantenimiento['mant_id'];

        //solicitud de cambio de fecha
        $res = $dbo->ExecuteQuery("SELECT IFNULL((
                                    SELECT
                                    mantenimiento_solicitud.maso_estado
                                    FROM
                                    rel_mantenimiento_solicitud
                                    INNER JOIN mantenimiento_solicitud ON (rel_mantenimiento_solicitud.maso_id = mantenimiento_solicitud.maso_id  AND
                                                                           mantenimiento_solicitud.maso_tipo='CAMBIO_FECHA_PROGRAMADA')
                                    WHERE rel_mantenimiento_solicitud.mant_id=$mant_id
                                    ORDER BY maso_fecha_solicitud ASC
                                    LIMIT 1),'NOSOLICITADA') AS mant_solicitud_cambio_fecha");
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        if(0<$res['rows']){
            $mantenimiento['mant_solicitud_cambio_fecha'] = $res['data'][0]['mant_solicitud_cambio_fecha'];
        }
        else{
            $mantenimiento['mant_solicitud_cambio_fecha'] = "NOSOLICITADA";
        }

        //solicitud de informe web
        $res = $dbo->ExecuteQuery("SELECT IFNULL((
                                    SELECT
                                    mantenimiento_solicitud.maso_estado
                                    FROM
                                    rel_mantenimiento_solicitud
                                    INNER JOIN mantenimiento_solicitud ON (rel_mantenimiento_solicitud.maso_id = mantenimiento_solicitud.maso_id  AND
                                                                           mantenimiento_solicitud.maso_tipo='INFORME_WEB')
                                    WHERE rel_mantenimiento_solicitud.mant_id=$mant_id
                                    ORDER BY maso_fecha_solicitud DESC
                                    LIMIT 1),'NOSOLICITADA') AS mant_solicitud_informe_web");
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        if(0<$res['rows']){
            $mantenimiento['mant_solicitud_informe_web'] = $res['data'][0]['mant_solicitud_informe_web'];
        }
        else{
            $mantenimiento['mant_solicitud_informe_web'] = "NOSOLICITADA";
        }



        $res = $dbo->ExecuteQuery("SELECT
                                        tarea.*
                                    FROM
                                        tarea
                                    WHERE
                                    tare_modulo='MNT' AND tare_estado IN ('CREADA','DESPACHADA')
                                    AND tare_id_relacionado = $mant_id
                                    ORDER BY tare_fecha_despacho DESC
                                    LIMIT 1");
        if ($res['status'] == 0){
            Flight::json(array("status" => 0, "error" => $res['error']));
            return;
        }
        if(0<$res['rows']){
            $mantenimiento['tarea'] = $res['data'][0];
        }
    }


    Flight::json($out);
});


//____________________________________________________________
//Crear
Flight::route('GET /contrato/@id:[0-9]+/mnt/add/filtros', function($id){
    $pais_id = Flight::get('PAIS_ID');
    $modu_id = Flight::get('MNT_MODU_ID');

    $out = array();
    $out['status'] = 1;
    $dbo = new MySQL_Database();

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CONTRATO'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['zonas'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='CLUSTER'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['clusters'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT zona_id, zona_nombre FROM zona WHERE cont_id=" . $id . " AND zona_estado = 'ACTIVO' AND zona_tipo='SUBGERENCIA'");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['sub_gerencias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT regi_id, regi_nombre FROM region WHERE pais_id=$pais_id ORDER BY regi_orden ASC");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['regiones'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT tecn_id, tecn_nombre FROM tecnologia");
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => "No se pudo obtener los filtros"));
    $out['tecnologias'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT e.empr_id, e.empr_nombre "
                            . "FROM empresa e, rel_contrato_empresa rce "
                            . "WHERE "
                                . "rce.cont_id=" . $id . " "
                                . "AND rce.coem_tipo='CONTRATISTA' "
                                . "AND e.empr_estado='ACTIVO' "
                                . "AND e.empr_id=rce.empr_id"
    );
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['empresa'] = $res['data'];


    $res = $dbo->ExecuteQuery("SELECT
                                    e.espe_id, e.espe_nombre
                                FROM especialidad e
                                INNER JOIN rel_modulo_especialidad reme ON(reme.espe_id = e.espe_id AND reme.modu_id=$modu_id)
                                WHERE e.espe_estado='ACTIVO' AND e.cont_id=" . $id);
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['especialidad'] = $res['data'];

    $res = $dbo->ExecuteQuery("SELECT
                                    f.form_id, f.form_nombre, reme.espe_id,refe.refe_preseleccion
                                FROM rel_modulo_especialidad reme
                                INNER JOIN rel_formulario_especialidad refe ON(refe.espe_id = reme.espe_id)
                                INNER JOIN formulario f ON(refe.form_id=f.form_id AND f.form_estado='ACTIVO' )
                                WHERE reme.modu_id=$modu_id");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>"No se pudo obtener los filtros"));
    $out['formulario'] = $res['data'];

    $out['status'] = 1;
    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/add', function($cont_id){
    //session_start();
    $peri_id                = Flight::get('PERI_ID_A_SOLICITUD');
    $form_ingreso           = Flight::get('FORM_INGRESO');
    $form_salida            = Flight::get('FORM_SALIDA');
    $usua_creador           = $_SESSION['user_id'];

    $db = new MySQL_Database();

    $empl_id                = mysql_real_escape_string($_POST['empl_id']);
    $empr_id                = mysql_real_escape_string($_POST['empr_id']);
    $espe_id                = mysql_real_escape_string($_POST['espe_id']);
    $mant_descripcion       = mysql_real_escape_string($_POST['mant_descripcion']);
    $mant_fecha_programada  = mysql_real_escape_string($_POST['mant_fecha_programada']);
    $form_id                = json_decode($_POST['form_id'],true);

    if(!is_array($form_id)){
        $form_id = array($form_id);
    }

    //Obtener contrato-período
    $res = $db->ExecuteQuery("SELECT
                                    rcpe_id
                                FROM rel_contrato_periodicidad
                                WHERE cont_id=$cont_id AND peri_id=$peri_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
    }

    if( $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"No se ha definido periodicidad 'A SOLICITUD' en este contrato"));
    }

    $rcpe_id = $res['data'][0]['rcpe_id'];

    $db->startTransaction();
    //crear mantenimiento período...
    $res = $db->ExecuteQuery("INSERT INTO mantenimiento_periodos SET
                             rcpe_id='$rcpe_id',
                             mape_fecha_pre_apertura = '$mant_fecha_programada',
                             mape_fecha_inicio = '$mant_fecha_programada',
                             mape_fecha_cierre = '$mant_fecha_programada',
                             mape_fecha_post_cierre = '$mant_fecha_programada',
                             mape_estado = 'PROCESANDO'");
    if(!$res['status']){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $mape_id = $res['data'][0]['id'];

    //crear mantenimiento...
    $res = $db->ExecuteQuery("INSERT INTO mantenimiento SET
                             empr_id='$empr_id',
                             cont_id='$cont_id',
                             mape_id='$mape_id',
                             empl_id='$empl_id',
                             espe_id='$espe_id',
                             clas_id=(SELECT clas_id FROM emplazamiento WHERE emplazamiento.empl_id='$empl_id'),
                             mant_fecha_programada='$mant_fecha_programada',
                             usua_creador='$usua_creador',
                             mant_descripcion='$mant_descripcion'");
    if(!$res['status']){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $mant_id = $res['data'][0]['id'];


    //crear mantemineito formularios...
    $values = "($mant_id,$form_ingreso),";
    foreach ($form_id as $f) {
        $values .= "($mant_id,$f),";
    }
    $values .= "($mant_id,$form_salida)";
    $res = $db->ExecuteQuery("INSERT INTO rel_mantenimiento_formulario (mant_id,form_id) VALUES $values");
    if(!$res['status']){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    //Subida de archivos
    if (isset($_FILES["archivos"])) {
        $files = array();
        foreach($_FILES["archivos"] as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $files[$key2][$key1] = $value2;
            }
        }
        $descriptions = json_decode($_POST['archivos_descripciones'],true);

        for($i=0; $i<count($files); $i++){
            $nombre      = $files[$i]['name'];
            $descripcion = $descriptions[$i];

            $filename = date('ymdHis')."_mnt_".$mant_id."_".str_replace(" ","_",$nombre);
            $resFile = Upload::UploadFile($files[$i],$filename);
            if( !$resFile['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resFile['error']));
                return;
            }

            $query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                        VALUES(
                            'DOCUMENTO',
                            '".$nombre."',
                            '".$descripcion."',
                            'mantenimiento',
                            $mant_id,
                            '".$resFile['data']['filename']."',
                            NOW(),
                            NULL,
                            $usua_creador
                    )";
            $resUpload = $db->ExecuteQuery($query);
            if( $resUpload['status']==0 ) {
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resUpload['error']));
                return;
            }
        }
    }

    $resEvent = Flight::AgregarEvento($db,"MNT","CREADA",$mant_id,null);
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();

    $res['data'][0]['id'] = $mant_id;
    Flight::json($res);
});

Flight::route('GET /contrato/@id:[0-9]+/mnt/get/@mant_id:[0-9]+', function($cont_id, $mant_id){
    $dbo = new MySQL_Database();

    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if( $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"No se pudo obtener mantenimiento $mant_id"));
        return;
    }

    $out['mantenimiento']=$res['data'][0];

    Flight::json($out);
});

Flight::route('GET|POST /contrato/@id:[0-9]+/mnt/upd/@ido:[0-9]+', function($id, $mant_id){

    $data = array_merge($_GET,$_POST);
    $usua_creador = $_SESSION['user_id'];

    $query = "UPDATE mantenimiento SET ".Flight::dataToUpdateString($data)." WHERE mant_id=".$mant_id;
    //Flight::json(array("status" => 0, "error" => $query));
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    Flight::json($res);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/del/@mant_id:[0-9]+', function($cont_id, $mant_id){

    $db = new MySQL_Database();
    $mant_observacion = mysql_real_escape_string($_POST['mant_observacion']);
    $db->startTransaction();
    $res = $db->ExecuteQuery("UPDATE mantenimiento
                                SET mant_estado='ANULADA', mant_observacion = '$mant_observacion'
                                WHERE cont_id= $cont_id AND mant_id = $mant_id");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
	

    //tarea
    $query = "UPDATE tarea SET tare_estado='ANULADA'
              WHERE tare_modulo='MNT' AND tare_id_relacionado = $mant_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //notificacion
    $query = "UPDATE notificacion SET noti_estado='ENTREGADA'
              WHERE
                    noti_modulo='MNT'
                    AND noti_estado='DESPACHADA'
                    AND noti_id_relacionado = $mant_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //evento
    $usua_id = 1;
    if (isset($_SESSION['user_id'])) {
        $usua_id = $_SESSION['user_id'];
    }

    $query = "UPDATE evento SET even_estado='ATENDIDO'
              WHERE
                    even_modulo='MNT'
                    AND even_estado='DESPACHADO'
                    AND even_id_relacionado = $mant_id";
    $res = $db->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $db->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //insertamos los eventos de anulacion
    $res = Flight::AgregarEvento($db,"MNT","ANULADA",$mant_id);
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $db->Commit();

    Flight::json($res);

});



//____________________________________________________________
//Adjuntar
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/adjuntar/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    //detalle
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if( count($res['data'])==0 ){
        Flight::json(array("status"=>0, "error"=>"Mantenimiento $mant_id no existe"));
        return;
    }

    $out['mantenimiento'] = $res['data'][0];

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/adjuntar/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $db = new MySQL_Database();
    $db->startTransaction();

    $usua_creador  = $_SESSION['user_id'];

    if (isset($_FILES["archivos"])) {
        $files = array();
        foreach($_FILES["archivos"] as $key1 => $value1) {
            foreach($value1 as $key2 => $value2) {
                $files[$key2][$key1] = $value2;
            }
        }
        $descriptions = json_decode($_POST['archivos_descripciones'],true);

        for($i=0; $i<count($files); $i++){
            $nombre      = $files[$i]['name'];
            $descripcion = $descriptions[$i];

            $filename = date('ymdHis')."_mnt_".$mant_id."_".str_replace(" ","_",$nombre);
            $resFile = Upload::UploadFile($files[$i],$filename);
            if( !$resFile['status'] ){
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resFile['error']));
                return;
            }

            $query = "INSERT INTO repositorio (repo_tipo_doc,repo_nombre,repo_descripcion,repo_tabla,repo_tabla_id,repo_ruta,repo_fecha_creacion,repo_data,usua_creador)
                        VALUES(
                            'DOCUMENTO',
                            '".$nombre."',
                            '".$descripcion."',
                            'mantenimiento',
                            $mant_id,
                            '".$resFile['data']['filename']."',
                            NOW(),
                            NULL,
                            $usua_creador
                    )";
            $resUpload = $db->ExecuteQuery($query);
            if( $resUpload['status']==0 ) {
                $db->Rollback();
                Flight::json(array("status" => 0, "error" => $resUpload['error']));
                return;
            }
        }
    }

    $db->Commit();

    Flight::json(array("status" => 1));
});

//____________________________________________________________
//Detalle
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/detalle/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    //detalle
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    if( count($res['data'])==0 ){
        Flight::json(array("status"=>0, "error"=>"Mantenimiento $mant_id no existe"));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];


    //asignacion
    $res = $dbo->ExecuteQuery("SELECT
                                    ma.maas_id,
                                    DATE_FORMAT(ma.maas_fecha_asignacion,'%d-%m-%Y %T') AS maas_fecha_asignacion,
                                    ma.usua_creador,
                                    usua_nombre,
                                    ma.maas_estado
                               FROM
                                    mantenimiento_asignacion ma
                                    INNER JOIN usuario ON (ma.usua_creador=usuario.usua_id)
                               WHERE
                                    mant_id=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['asignacion'] = $res['data'];

    if(0<$res['rows']){
        for($i=0;$i<count($out['asignacion']);$i++){
            $maas_id = $out['asignacion'][$i]['maas_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            usua_nombre,
                                            rmau_tipo
                                       FROM
                                            rel_mantenimiento_asignacion_usuario
                                            INNER JOIN usuario ON (rel_mantenimiento_asignacion_usuario.usua_id=usuario.usua_id)
                                       WHERE
                                            maas_id=$maas_id");

            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            //$detalle = $res['data'];
            $detalle = array("jefe"=>"","acompanantes"=>array());
            if(0<$res['rows']){
                for($j=0;$j<$res['rows'];$j++){
                    if($res['data'][$j]['rmau_tipo']=="JEFECUADRILLA"){
                        $detalle['jefe'] = $res['data'][$j]['usua_nombre'];
                    }
                    else{
                        array_push($detalle['acompanantes'],$res['data'][$j]['usua_nombre']);
                    }
                }
            }

            $out['asignacion'][$i]['detalle'] = $detalle;
        }
    }


    //visitas
    $res = $dbo->ExecuteQuery("SELECT
                                tare_id,
                                usua_nombre,
                                tare_fecha_despacho,
                                tare_fecha_descarga,
                                tare_estado
                              FROM tarea
                              INNER JOIN usuario ON (usuario.usua_id=tarea.usua_id)
                              WHERE tare_modulo='MNT' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['visitas'] = $res['data'];

    //visitas detalle
    for($i=0;$i<count($out['visitas']);$i++){
        $tare_id = $out['visitas'][$i]['tare_id'];
        $res = $dbo->ExecuteQuery("SELECT
                                        rel_tarea_formulario_respuesta.*
                                      FROM rel_tarea_formulario_respuesta
                                      WHERE tare_id=$tare_id");
        if( $res['status']==0 ){
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
        $out['visitas'][$i]['detalle'] = $res['data'];
    }


    //informe
    $res = $dbo->ExecuteQuery("SELECT
                                info_id,
                                DATE_FORMAT(info_fecha_creacion,'%d-%m-%Y %T') AS info_fecha_creacion,
                                uc.usua_nombre AS usua_creador,
                                info_estado,
                                uv.usua_nombre AS usua_validador,
                                DATE_FORMAT(info_fecha_validacion,'%d-%m-%Y %T') AS info_fecha_validacion,
                                info_observacion
                               FROM
                                informe
                               INNER JOIN usuario uc ON (informe.usua_creador=uc.usua_id)
                               LEFT JOIN usuario uv ON (informe.usua_validador=uv.usua_id)
                               WHERE info_modulo='MNT' AND info_id_relacionado=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['informes'] = $res['data'];


    //eventos
    $res = $dbo->ExecuteQuery("SELECT
                                even_evento,
                                even_fecha,
                                even_estado,
                                even_comentario
                               FROM
                                evento
                               WHERE
                                even_modulo='MNT' AND even_id_relacionado=$mant_id
                               ORDER BY even_fecha DESC");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['eventos'] = $res['data'];

    Flight::json($out);
});

//____________________________________________________________
//Asignacion
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/asignacion/@mant_id:[0-9]+', function($cont_id,$mant_id){
    //session_start();

    $usua_id = $_SESSION['user_id'];
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    //Obtener asignación previa (si la hay)
    $res = $dbo->ExecuteQuery("SELECT
                                mantenimiento_asignacion.maas_id,
                                usuario.usua_nombre,
                                maas_fecha_asignacion,
                                emplazamiento_visita.emvi_fecha_ingreso
                               FROM
                                mantenimiento_asignacion
                               INNER JOIN rel_mantenimiento_asignacion_usuario ON (mantenimiento_asignacion.maas_id=rel_mantenimiento_asignacion_usuario.maas_id AND rmau_tipo='JEFECUADRILLA')
                               INNER JOIN usuario ON (rel_mantenimiento_asignacion_usuario.usua_id=usuario.usua_id)
                               LEFT JOIN emplazamiento_visita ON (emplazamiento_visita.emvi_modulo='MNT' AND emplazamiento_visita.emvi_id_relacionado=mant_id AND emplazamiento_visita.emvi_estado='ACTIVO')
                               WHERE
                                mant_id=$mant_id AND maas_estado='ACTIVO'
                               ORDER BY maas_fecha_asignacion DESC
                               LIMIT 1");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['asignacion'] = null;
    if(0<$res['rows']){
        $out['asignacion'] = $res['data'][0];
    }

    $res = $dbo->ExecuteQuery("SELECT
                                 usuario.usua_id,
                                 usuario.usua_nombre,
                                 perfil.perf_nombre
                              FROM
                                 mantenimiento
                              INNER JOIN usuario ON (usuario.empr_id=mantenimiento.empr_id)
                              INNER JOIN rel_contrato_usuario ON (rel_contrato_usuario.usua_id=usuario.usua_id AND rel_contrato_usuario.cont_id=mantenimiento.cont_id)
                              INNER JOIN rel_usuario_perfil ON (rel_usuario_perfil.usua_id=usuario.usua_id)
                              INNER JOIN perfil ON (perfil.perf_id=rel_usuario_perfil.perf_id AND perfil.perf_nombre IN ('JEFE_CUADRILLA','TECNICO'))
                              WHERE
                                    mant_id=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['jefes'] = array();
    $out['acompanantes'] = array();
    foreach ($res['data'] as $usuario) {
        if ($usuario['perf_nombre'] == 'TECNICO') {
            $out['acompanantes'][] = $usuario;
        } else {
            $out['jefes'][] = $usuario;
        }
    }

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/asignacion/@mant_id:[0-9]+', function($cont_id,$mant_id){
    //session_start();

    $usua_creador = $_SESSION['user_id'];
    $dbo = new MySQL_Database();
    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("INSERT INTO mantenimiento_asignacion SET
                                mant_id=$mant_id,
                                maas_fecha_asignacion=NOW(),
                                usua_creador='$usua_creador',
                                maas_estado='ACTIVO'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $maas_id = $res['data'][0]['id'];

    $jefe = $_POST['jefecuadrilla'];
    $res = $dbo->ExecuteQuery("INSERT INTO rel_mantenimiento_asignacion_usuario SET
                                maas_id=$maas_id,
                                usua_id=$jefe,
                                rmau_tipo='JEFECUADRILLA'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }


    $acompanantes = array();
    if(is_array($_POST['acompanantes'])){
        foreach($_POST['acompanantes'] as $a){
            array_push($acompanantes,"(".$maas_id.",".$a.",'ACOMPANANTE')");
        }
        $acompanantes = implode(",",$acompanantes);
    }
    else{
        $acompanantes = "(".$maas_id.",".$_POST['acompanantes'].",'ACOMPANANTE')";
    }

    $res = $dbo->ExecuteQuery("INSERT INTO rel_mantenimiento_asignacion_usuario
                                (maas_id,usua_id,rmau_tipo)
                                VALUES
                                $acompanantes");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }


    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"MNT","ASIGNAR",$mant_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }


    $resEvent = Flight::AgregarEvento($dbo,"MNT","ASIGNADA",$mant_id,array("maas_id"=>$maas_id));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();

    Flight::json($res);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/asignacion/@mant_id:[0-9]+/cancelar/@maas_id:[0-9]+', function($cont_id,$mant_id,$maas_id){
    $db = new MySQL_Database();
    $db->startTransaction();

    $res = $db->ExecuteQuery("UPDATE mantenimiento_asignacion SET
                                maas_estado='NOACTIVO' WHERE maas_id=$maas_id");
    if( $res['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($db,"MNT","ASIGNACION_CANCELADA",$mant_id,array("maas_id"=>$maas_id));
    if( $resEvent['status']==0 ){
        $db->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $db->Commit();
    Flight::json($res);

});


//____________________________________________________________
//Visita
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/visita/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    //Revisar si se tiene aprobación
    $res = $dbo->ExecuteQuery("SELECT
                                    maso_estado
                                FROM
                                    mantenimiento_solicitud
                                INNER JOIN rel_mantenimiento_solicitud ON (rel_mantenimiento_solicitud.maso_id = mantenimiento_solicitud.maso_id
                                                                            AND rel_mantenimiento_solicitud.mant_id = $mant_id)
                                WHERE maso_tipo='INFORME_WEB'");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if( $res['rows']==0 || $res['data'][0]['maso_estado'] != 'APROBADA'){
        Flight::json(array("status"=>0, "error"=>"Se requiere de aprobación para ingresar visita via web"));
        return;
    }

    //Revisar si MNT ya está asignado
    $res = $dbo->ExecuteQuery("SELECT
                                    maas_id
                               FROM
                                    mantenimiento_asignacion
                               WHERE
                                    mant_id=$mant_id AND maas_estado='ACTIVO'");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    if( $res['rows']==0 ){
        Flight::json(array("status"=>0, "error"=>"Se requiere haber asignado MNT antes de ingresar visita via web"));
        return;
    }


    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    //Cargar formularios
    $forms = array();
    $res = $dbo->ExecuteQuery("SELECT
                                formulario.form_id AS form_id,
                                form_nombre AS form_name,
                                form_opciones AS form_options
                              FROM rel_mantenimiento_formulario
                              INNER JOIN formulario ON (formulario.form_id=rel_mantenimiento_formulario.form_id)
                              WHERE mant_id=$mant_id AND form_estado='ACTIVO'
                              ORDER BY rmaf_id");
    if($res['status']==0){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    if(0<$res['rows']){
        $forms = $res['data'];

        foreach($forms as &$form){
            $form_id = $form['form_id'];
            if($form['form_options']!=""){
              $form['form_options'] = json_decode($form['form_options'],true);
            }

            $res = $dbo->ExecuteQuery("SELECT
                                fogr_id         AS fopa_id,
                                fogr_nombre     AS fopa_name,
                                fogr_opciones   AS fopa_options
                              FROM
                                formulario_grupo
                              WHERE form_id = '$form_id'  AND fogr_estado='ACTIVO'");
            if($res['status']==0){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            $form['pages'] = $res['data'];

            foreach($form['pages'] as &$page){
                $fopa_id = $page['fopa_id'];

                $res = $dbo->ExecuteQuery("SELECT
                                foit_id         AS foco_id,
                                foit_tipo       AS foco_type,
                                foit_nombre     AS foco_label,
                                foit_requerido  AS foco_required,
                                foit_opciones   AS foco_options
                              FROM
                                formulario_item
                              WHERE fogr_id = '$fopa_id'  AND foit_estado='ACTIVO'
                              ORDER BY foit_orden,foit_id");
                if($res['status']==0){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }
                $page['controls'] = $res['data'];

                foreach($page['controls'] as &$control){
                    if($control['foco_options']!=""){
                        $control['foco_options'] = json_decode($control['foco_options'],true);
                    }
                }


            }

        }
    }
    $out['forms'] = $forms;

    //Ruta para guardar el formulario (para hacerlo funcionar en distintos modulos)
    $out['route'] = "/contrato/".$cont_id."/mnt/visita/".$mant_id;

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/visita/@mant_id:[0-9]+', function($cont_id,$mant_id){
  $db = new MySQL_Database();

  $user_id        = $_SESSION['user_id'];
  $form_id        = mysql_real_escape_string($_POST['form_id']);
  $form_name      = mysql_real_escape_string($_POST['form_name']);
  $foan_datetime  = mysql_real_escape_string($_POST['foan_datetime_saved']);
  $status         = mysql_real_escape_string($_POST['status']);
  $last           = mysql_real_escape_string($_POST['last']);

  $db->startTransaction();

  $res = $db->ExecuteQuery("INSERT INTO formulario_respuesta SET
                              form_id=$form_id,
                              usua_creador=$user_id,
                              fore_fecha_creacion='$foan_datetime',
                              fore_fecha_recepcion=NOW(),
                              fore_ubicacion='web'");
  if($res['status']){
    $foan_id = $res['data'][0]['id'];

    //actualizar datos
    $data    = json_decode(utf8_encode($_POST['form_data']),true);
    if($data){
        //Revisar data para subir imagenes
        $values = array();
        foreach($data as $key => &$value){
            if(is_array($value)){
              /*
              foreach($value as &$v){

                if(isset($v['image'])){
                  if (isset($_FILES[$key])) {
                    foreach ($_FILES[$key]['name'] as $k => $n) {
                      if($v['image']==$n){
                        $v['image'] = "web_".date("Ymd_His").".".microtime(true)."_photo.jpg";

                        $file_path = "../uploads/".$v['image'];
                        if(!move_uploaded_file($_FILES[$key]['tmp_name'][$k], $file_path)) {
                           $db->Rollback();
                           Flight::json(array("status"=>false,"error"=>"Error al cargar archivo '$n'"));
                        }
                        break;
                      }
                    }
                  }
                }
              }
              */
              array_walk_recursive($value, function(&$item,$curKey,$foco_id) {
                    if($curKey==="image"){
                        if (isset($_FILES[$foco_id])) {
                            foreach ($_FILES[$foco_id]['name'] as $k => $n) {
                                if($item==$n){
                                    $item = "web_".date("Ymd_His").".".microtime(true)."_photo.jpg";

                                    $file_path = "../uploads/".$item;
                                    if(!move_uploaded_file($_FILES[$foco_id]['tmp_name'][$k], $file_path)) {
                                        $db->Rollback();
                                        Flight::json(array("status"=>false,"error"=>"Error al cargar archivo '$n'"));
                                    }
                                    break;
                                 }
                            }
                        }
                    }
                },$key);




              $value = json_encode($value);
            }

            array_push($values,"(0,$foan_id,$key,'$value')");
        }

        //Agregar valores a formulario
        $query = "INSERT INTO formulario_valor VALUES ".implode(",",$values);
        $res = $db->ExecuteQuery($query);
        if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
        }

        //Obtener tarea relacionada
        $res = $db->ExecuteQuery("SELECT
                                    tare_id
                                  FROM tarea
                                  WHERE tare_modulo='MNT' AND tare_tipo='VISITAR_SITIO' AND tare_id_relacionado=$mant_id
                                  ORDER BY tare_fecha_despacho DESC
                                  LIMIT 1");
        if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
        }
        $tare_id = "";
        if(0<$res['rows']){
          $tare_id = $res['data'][0]['tare_id'];
        }

        if($tare_id!=""){
          $res = $db->ExecuteQuery("SELECT
                                      rtfr_id
                                    FROM
                                      rel_tarea_formulario_respuesta
                                    WHERE
                                      tare_id=$tare_id AND form_id=$form_id");
          if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
          }
          $rtfr_id = "";
          if(0<$res['rows']){
            $rtfr_id = $res['data'][0]['rtfr_id'];
          }

          if($rtfr_id==""){
            $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta SET
                                        tare_id=$tare_id,
                                        form_id=$form_id,
                                        fore_id=$foan_id,
                                        rtfr_accion='$form_name',
                                        rtfr_fecha='$foan_datetime',
                                        rtfr_estado='$status'");
            if(!$res['status']){
              $db->Rollback();
              Flight::json(array("status"=>false,"error"=>$res['error']));
            }
            $rtfr_id = $res['data'][0]['id'];
          }
          else{
            $res = $db->ExecuteQuery("UPDATE rel_tarea_formulario_respuesta SET
                                        fore_id=$foan_id,
                                        rtfr_fecha='$foan_datetime',
                                        rtfr_estado='$status'
                                        WHERE rtfr_id='$rtfr_id'");
            if(!$res['status']){
              $db->Rollback();
              Flight::json(array("status"=>false,"error"=>$res['error']));
            }
          }


          $res = $db->ExecuteQuery("INSERT INTO rel_tarea_formulario_respuesta_revisiones SET
                                rtfr_id=$rtfr_id,
                                fore_id=$foan_id,
                                rfrr_fecha='$foan_datetime',
                                rfrr_estado='$status'");
          if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
          }
        }

        if($last=="1"){ //crear informe, enviar evento, finalizar tarea
          $res = $db->ExecuteQuery("INSERT INTO informe SET
                                      info_modulo='MNT',
                                      info_id_relacionado='$mant_id',
                                      usua_creador='$user_id',
                                      info_fecha_creacion=NOW(),
                                      info_estado='SINVALIDAR',
                                      info_data='{\"tare_id\":$tare_id}'");
          if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
          }
          $info_id = $res['data'][0]['id'];

          $res = Flight::AgregarEvento($db,"MNT","INFORME_AGREGADO",$mant_id,array("info_id"=>$info_id));
          if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
          }

          $res = Flight::FinalizarTareaRelacionada($db,"MNT","VISITAR_SITIO",$mant_id);
          if(!$res['status']){
            $db->Rollback();
            Flight::json(array("status"=>false,"error"=>$res['error']));
            return;
          }
      }

      $db->Commit();
      Flight::json(array("status"=>true,"data"=>$foan_id));
    }
    else{
        $db->Rollback();
        Flight::json(array("status"=>false,"data"=>"Sin datos de formulario"));
        return;
    }
  }
  else{
      $db->Rollback();
      Flight::json(array("status"=>false,"error"=>$res['error']));
      return;
  }

});


//____________________________________________________________
//Informe
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/informe/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    Flight::json($out);

});

Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/informe/@mant_id:[0-9]+/ver/@info_id:[0-9]+', function($cont_id,$mant_id,$info_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();

    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    $res = $dbo->ExecuteQuery("SELECT
                                    usua_nombre,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data
                                FROM
                                    informe
                                INNER JOIN usuario ON (usuario.usua_id=informe.usua_creador)
                                WHERE
                                    info_modulo='MNT' AND info_id=$info_id AND info_id_relacionado=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['informe'] = array();
    if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id
                                        GROUP BY fore_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }

                $res = $dbo->ExecuteQuery("SELECT
                                                fogr_nombre,
                                                formulario_item.foit_id,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }

                $data = array();

                foreach ($res['data'] as &$row) {
                    $row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);

                    if(!isset($data[$row['fogr_nombre']])){
                        $data[$row['fogr_nombre']] = array();
                    }
                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }
                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($data[$row['fogr_nombre']],$row);
                }
                $out['formularios'][$i]['data'] = $data;
            }

        }
    }

    Flight::json($out);
});


Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/informe/@mant_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$mant_id,$info_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    /*
     //Chequear si ya fue validado
    $res = $dbo->ExecuteQuery("SELECT
                                info_estado
                               FROM
                                informe
                               WHERE info_id=$info_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $info_estado = $res['data'][0]['info_estado'];
    if($info_estado!="SINVALIDAR"){
        Flight::json(array("status"=>0, "error"=>"Informe ya fue validado"));
        return;
    }
    */

    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    $res = $dbo->ExecuteQuery("SELECT
                                    usua_nombre,
                                    info_id,
                                    info_fecha_creacion,
                                    info_data
                                FROM
                                    informe
                                INNER JOIN usuario ON (usuario.usua_id=informe.usua_creador)
                                WHERE
                                    info_modulo='MNT' AND info_id=$info_id AND info_id_relacionado=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $out['informe'] = array();
    if(0<$res['rows']){
        $out['informe'] = $res['data'][0];

        $out['formularios'] = array();

        //Obtener visitas
        $info_data = json_decode($out['informe']['info_data'],true);
        if($info_data!=null){
            $tare_id = $info_data['tare_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                            rel_tarea_formulario_respuesta_revisiones.fore_id,
                                            fore_ubicacion,
                                            rtfr_accion,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_fecha,
                                            rel_tarea_formulario_respuesta_revisiones.rfrr_estado
                                        FROM
                                            rel_tarea_formulario_respuesta
                                        INNER JOIN rel_tarea_formulario_respuesta_revisiones ON (rel_tarea_formulario_respuesta.rtfr_id = rel_tarea_formulario_respuesta_revisiones.rtfr_id)
                                        INNER JOIN formulario_respuesta ON (formulario_respuesta.fore_id = rel_tarea_formulario_respuesta_revisiones.fore_id)
                                        WHERE
                                            tare_id=$tare_id");
            if( $res['status']==0 ){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }

            $out['formularios'] = $res['data'];

            for($i=0;$i<count($out['formularios']);$i++){
                $fore_id = $out['formularios'][$i]['fore_id'];

                if($out['formularios'][$i]['fore_ubicacion']!="" && $out['formularios'][$i]['fore_ubicacion']!="web"){
                    $out['formularios'][$i]['fore_ubicacion'] = json_decode($out['formularios'][$i]['fore_ubicacion'],true);
                }


                $res = $dbo->ExecuteQuery("SELECT
                                                fogr_nombre,
                                                formulario_item.foit_id,
                                                foit_nombre,
                                                foit_tipo,
                                                fova_valor,
                                                foit_opciones
                                            FROM
                                                formulario_respuesta
                                            INNER JOIN formulario_grupo ON (formulario_grupo.form_id=formulario_respuesta.form_id)
                                            INNER JOIN formulario_item ON (formulario_item.fogr_id=formulario_grupo.fogr_id)
                                            LEFT JOIN formulario_valor ON (formulario_valor.foit_id=formulario_item.foit_id AND formulario_valor.fore_id=formulario_respuesta.fore_id)
                                            WHERE
                                                formulario_respuesta.fore_id=$fore_id AND foit_tipo NOT IN ('LABEL','SAVE') AND foit_estado='ACTIVO'
                                            ORDER BY fogr_orden,foit_orden;");
                if( $res['status']==0 ){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }

                $data = array();

                foreach ($res['data'] as &$row) {
                    $row['fova_valor'] = $row['fova_valor'];//utf8_encode($row['fova_valor']);

                    if(!isset($data[$row['fogr_nombre']])){
                        $data[$row['fogr_nombre']] = array();
                    }
                    if($row['foit_tipo']=="CAMERA"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }
                    if($row['foit_tipo']=="AGGREGATOR"){
                        $row['fova_valor'] = json_decode($row['fova_valor'],true);
                    }

                    if($row['foit_opciones']!=""){
                        $row['foit_opciones'] = json_decode($row['foit_opciones'],true);
                    }

                    array_push($data[$row['fogr_nombre']],$row);
                }
                $out['formularios'][$i]['data'] = $data;
            }

        }
    }

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/informe/@mant_id:[0-9]+/validar/@info_id:[0-9]+', function($cont_id,$mant_id,$info_id){
    //session_start();

    $info_estado      = $_POST['info_estado'];
    $info_observacion = $_POST['info_observacion'];
    $usua_validador   = $_SESSION['user_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("UPDATE informe SET
                                info_estado='$info_estado',
                                info_observacion='$info_observacion',
                                usua_validador='$usua_validador',
                                info_fecha_validacion=NOW()
                                WHERE
                                info_id='$info_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"MNT","VALIDAR_INFORME",$mant_id,'"info_id":'.$info_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }


    $resEvent = Flight::AgregarEvento($dbo,"MNT","INFORME_VALIDADO",$mant_id,array("info_id"=>$info_id,"info_estado"=>$info_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);
});

//____________________________________________________________
//Visita
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/visita/@mant_id:[0-9]+', function($cont_id,$mant_id){
    global $DEF_CONFIG;
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    /*
    $res = $dbo->ExecuteQuery("SELECT espe_id, espe_nombre FROM especialidad WHERE espe_estado='ACTIVO'");
    if( $res['status']==0 ) Flight::json(array("status"=>0, "error"=>$res['error']));
    $out['especialidad'] = $res['data'];
    */

    $forms = array();
    /*
    $form_ids = array_keys($DEF_CONFIG["os"]["formularios"]);
    $form_ids_str = implode("','",$form_ids);
    //Cargar formularios
    $res = $dbo->ExecuteQuery("SELECT
                                form_id     AS form_id,
                                form_nombre AS form_name
                              FROM
                                formulario
                              WHERE form_id IN ('$form_ids_str') AND form_estado='ACTIVO'");
    if($res['status']==0){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    if(0<$res['rows']){
        $forms = $res['data'];

        foreach($forms as &$form){
            $form_id = $form['form_id'];

            $res = $dbo->ExecuteQuery("SELECT
                                fogr_id         AS fopa_id,
                                fogr_nombre     AS fopa_name,
                                fogr_opciones   AS fopa_options
                              FROM
                                formulario_grupo
                              WHERE form_id = '$form_id'  AND fogr_estado='ACTIVO'");
            if($res['status']==0){
                Flight::json(array("status"=>0, "error"=>$res['error']));
                return;
            }
            $form['pages'] = $res['data'];

            foreach($form['pages'] as &$page){
                $fopa_id = $page['fopa_id'];

                $res = $dbo->ExecuteQuery("SELECT
                                foit_id         AS foco_id,
                                foit_tipo       AS foco_type,
                                foit_nombre     AS foco_label,
                                foit_requerido  AS foco_required,
                                foit_opciones   AS foco_options
                              FROM
                                formulario_item
                              WHERE fogr_id = '$fopa_id'  AND foit_estado='ACTIVO'
                              ORDER BY foit_orden,foit_id");
                if($res['status']==0){
                    Flight::json(array("status"=>0, "error"=>$res['error']));
                    return;
                }
                $page['controls'] = $res['data'];

                foreach($page['controls'] as &$control){
                    if($control['foco_options']!=""){
                        $control['foco_options'] = json_decode($control['foco_options'],true);
                    }
                }


            }

        }
    }
    */
    $out['forms'] = $forms;



    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/os/visita/@orse_id:[0-9]+', function($cont_id,$orse_id){


});

//____________________________________________________________
//Solicitudes
Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/solicitud/informe/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $razon        = $_POST['razon'];
    $usua_creador = $_SESSION['user_id'];

    $dbo = new MySQL_Database();
    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("INSERT INTO mantenimiento_solicitud SET
                                maso_tipo='INFORME_WEB',
                                maso_razon='$razon',
                                usua_creador=$usua_creador,
                                maso_fecha_solicitud=NOW(),
                                maso_data='',
                                maso_estado='SOLICITANDO'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $maso_id = $res['data'][0]['id'];


    $res = $dbo->ExecuteQuery("INSERT INTO rel_mantenimiento_solicitud SET
                                mant_id=$mant_id,
                                maso_id=$maso_id");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($dbo,"MNT","SOLICITUD_INFORME",$mant_id,array("maso_id"=>$maso_id,"razon"=>$razon));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();

    Flight::json($res);
});


Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/solicitud/informe/@mant_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$mant_id,$tare_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];


    $res = $dbo->ExecuteQuery("SELECT tare_id,tare_data
                               FROM tarea
                               WHERE  tare_id='$tare_id'");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['tarea'] = $res['data'][0];
    if($out['tarea']['tare_data']!=""){
       $out['tarea']['tare_data'] = json_decode($out['tarea']['tare_data'],true);
    }

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/solicitud/informe/@mant_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$mant_id,$tare_id){
    $usua_validador         = $_SESSION['user_id'];
    $maso_estado            = $_POST['mant_solicitud_informe_web'];
    $maso_id                = $_POST['maso_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("UPDATE mantenimiento_solicitud SET
                                usua_validador=$usua_validador,
                                maso_fecha_validacion=NOW(),
                                maso_estado='$maso_estado'
                                WHERE
                                maso_id='$maso_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"MNT","VALIDAR_SOLICITUD_INFORME",$mant_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }


    $resEvent = Flight::AgregarEvento($dbo,"MNT","SOLICITUD_INFORME_VALIDADA",$mant_id,array("maso_id"=>$maso_id,"mant_solicitud_informe_web"=>$maso_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);


});



Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/solicitud/cambio_fecha_programada/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $fecha        = $_POST['fecha'];
    $razon        = $_POST['razon'];
    $usua_creador = $_SESSION['user_id'];
    $data         = array("nueva_fecha_programada"=>$fecha);

    $dbo = new MySQL_Database();
    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("SELECT mant_fecha_programada
                                FROM mantenimiento
                                WHERE mant_id=$mant_id");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $data["actual_fecha_programada"] = $res['data'][0]['mant_fecha_programada'];
    $data = json_encode($data);

    $res = $dbo->ExecuteQuery("INSERT INTO mantenimiento_solicitud SET
                                maso_tipo='CAMBIO_FECHA_PROGRAMADA',
                                maso_razon='$razon',
                                usua_creador=$usua_creador,
                                maso_fecha_solicitud=NOW(),
                                maso_data='$data',
                                maso_estado='SOLICITANDO'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $maso_id = $res['data'][0]['id'];


    $res = $dbo->ExecuteQuery("INSERT INTO rel_mantenimiento_solicitud SET
                                mant_id=$mant_id,
                                maso_id=$maso_id");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($dbo,"MNT","SOLICITUD_CAMBIO_FECHA_PROGRAMADA",$mant_id,array("maso_id"=>$maso_id,"nueva_fecha_programada"=>$fecha,"razon"=>$razon));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();

    Flight::json($res);
});
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/solicitud/cambio_fecha_programada/@mant_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$mant_id,$tare_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];


    $res = $dbo->ExecuteQuery("SELECT tare_id,tare_data
                               FROM tarea
                               WHERE  tare_id='$tare_id'");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['tarea'] = $res['data'][0];
    if($out['tarea']['tare_data']!=""){
       $out['tarea']['tare_data'] = json_decode($out['tarea']['tare_data'],true);
    }

    Flight::json($out);
});

Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/solicitud/cambio_fecha_programada/@mant_id:[0-9]+/validar/@tare_id:[0-9]+', function($cont_id,$mant_id,$tare_id){
    //session_start();
    $usua_validador         = $_SESSION['user_id'];
    $mant_fecha_programada  = $_POST['mant_fecha_programada'];
    $maso_estado            = $_POST['mant_solicitud_cambio_fecha_programada'];
    $maso_id                = $_POST['maso_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    if($maso_estado=="APROBADA"){
        $res = $dbo->ExecuteQuery("UPDATE mantenimiento SET
                                mant_fecha_programada='$mant_fecha_programada'
                                WHERE
                                mant_id='$mant_id'");
        if( $res['status']==0 ){
            $dbo->Rollback();
            Flight::json(array("status"=>0, "error"=>$res['error']));
            return;
        }
    }


    $res = $dbo->ExecuteQuery("UPDATE mantenimiento_solicitud SET
                                usua_validador=$usua_validador,
                                maso_fecha_validacion=NOW(),
                                maso_estado='$maso_estado'
                                WHERE
                                maso_id='$maso_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"MNT","VALIDAR_SOLICITUD_CAMBIO_FECHA_PROGRAMADA",$mant_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }


    $resEvent = Flight::AgregarEvento($dbo,"MNT","SOLICITUD_CAMBIO_FECHA_PROGRAMADA_VALIDADA",$mant_id,array("maso_id"=>$maso_id,"mant_solicitud_cambio_fecha_programada"=>$maso_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);
});



//____________________________________________________________
//Cerrar
Flight::route('GET /contrato/@cont_id:[0-9]+/mnt/cerrar/@mant_id:[0-9]+', function($cont_id,$mant_id){
    $out = array();
    $out['status'] = 1;

    $dbo = new MySQL_Database();
    $res = Flight::ObtenerDetalleMantenimiento($dbo,$cont_id,$mant_id);
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['mantenimiento'] = $res['data'][0];

    $res = $dbo->ExecuteQuery("SELECT
                                    info_id,
                                    uc.usua_nombre AS usua_creador,
                                    info_fecha_creacion,
                                    uv.usua_nombre AS usua_validador,
                                    info_fecha_validacion,
                                    info_estado,
                                    info_observacion
                                FROM
                                    informe
                                INNER JOIN usuario uc ON (uc.usua_id = informe.usua_creador)
                                LEFT JOIN usuario uv ON (uv.usua_id = informe.usua_validador)
                                WHERE info_modulo='MNT' AND info_id_relacionado=$mant_id");
    if( $res['status']==0 ){
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }
    $out['informe'] = $res['data'];


    Flight::json($out);
});


Flight::route('POST /contrato/@cont_id:[0-9]+/mnt/cerrar/@mant_id:[0-9]+', function($cont_id,$mant_id){
    //session_start();
    $mant_estado      = $_POST['mant_estado'];
    $usua_validador   = $_SESSION['user_id'];

    $dbo = new MySQL_Database();

    $dbo->startTransaction();

    $res = $dbo->ExecuteQuery("UPDATE mantenimiento SET
                                mant_estado='$mant_estado',
                                usua_validador='$usua_validador',
                                mant_fecha_validacion=NOW()
                               WHERE
                                mant_id='$mant_id'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $res = $dbo->ExecuteQuery("UPDATE informe SET
                                info_estado='APROBADO'
                                WHERE
                                info_modulo='MNT' AND info_id_relacionado='$mant_id' AND info_estado='PREAPROBADO'");
    if( $res['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$res['error']));
        return;
    }

    $resEvent = Flight::AgregarEvento($dbo,"MNT","MNT_VALIDADA",$mant_id,array("mant_estado"=>$mant_estado));
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }

    $resEvent = Flight::FinalizarTareaRelacionada($dbo,"MNT","VALIDAR_MNT",$mant_id);
    if( $resEvent['status']==0 ){
        $dbo->Rollback();
        Flight::json(array("status"=>0, "error"=>$resEvent['error']));
        return;
    }


    //cerrar tareas
    $query = "UPDATE tarea SET tare_estado='CANCELADA'
              WHERE tare_modulo='MNT' AND tare_id_relacionado = $mant_id";
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    //cerrar notificaciones
    $query = "UPDATE notificacion SET noti_estado='ENTREGADA'
              WHERE
                    noti_modulo='MNT'
                    AND noti_estado='DESPACHADA'
                    AND noti_id_relacionado = $mant_id";
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) {
        $dbo->Rollback();
        Flight::json(array("status" => 0, "error" => $res['error']));
        return;
    }

    $dbo->Commit();
    Flight::json($res);
});


?>
