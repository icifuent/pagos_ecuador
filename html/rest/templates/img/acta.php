<?php
function renderActa($data){
	$contrato = $data['contrato'];
	$empresa =$data['empresa'];
	$fecha = $data['fecha'];
	$mes =$data['mes'];	
	$ano =$data['ano'];	
	$mesAnterior =$data['mesAnterior'];	
	$anoAnterior =$data['anoAnterior'];	
	$total=$data['total'];	
	$aprobacionesActa=$data['AprobadasMes'];
	$aprobacionesTotal=$data['AprobadasTotal'];
	$nomUsuario="Miguel Tamb.";
	$estadoVal=$data['estadoVal'];
	return '

<img src="/var/www/siompagos.movistar.cl/html/rest/templates/img/logo_telefonica2.jpg">
<br>
<table border="0px" width="100%">
	<tr>
		<td width="15%"></td>
		<td ALIGN="center" style="height:auto; width:70%;"  ><FONT SIZE=3><strong>ACTA DE ACEPTACIÓN CONFORME</strong></FONT></td>
		<td width="15%"></td>
	</tr>
	<tr>
		<td width="15%"></td>
		<td ALIGN="center" style="height:auto; width:70%" ><FONT SIZE=3><strong>CONTRATO INTEGRADOR DE MANTENIMIENTO '.$contrato.' </strong></FONT></td>
		<td width="15%"></td>
	</tr>
</table>
<br>
<table border="0px" width="100%">
	<tr>
		<td width="20%">MATERIA:</td>
		<td style="height:auto; width:85%" >Aceptación Conforme del Proceso 14650520, Materia CR.14650520-1781-TMCH-Contrato Integrador de Mantenimiento '. $contrato.'.
		</td>
	</tr>
	<tr>
		<td>NÚM. DE CONTRATO:</td>
		<td>3300078039</td>
	</tr>
	<tr>
		<td>FECHA:</td>
		<td>'.$fecha.'</td>
	</tr>
</table>
<table border="0px" >
	<tr>
		<td width="20%"><p><FONT SIZE=3>I	OBJETIVO</FONT></p></td>
		<td style="height:auto; width:80%" ></td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:auto; width:80%" >Dejar establecida la Aceptación Conforme del Servicio del contrato integrador de mantenimiento '.$contrato.',  para el mes de '.$mes.' '.$ano.'.</td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:15px; width:80%" ></td>
	</tr>
	<tr>
		<td width="20%"><p><FONT SIZE=3>II	GENERAL</FONT></p></td>
		<td style="height:auto; width:80%" ></td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:auto; width:80%">1. '.$empresa.' proporciono este Servicio en conformidad a lo estipulado, realizando la cantidad de '.$aprobacionesTotal.' mantenimientos preventivos, aprobados y finalizados. '.$aprobacionesActa.'</td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:auto; width:80%">2. El cumplimiento anterior, se basa en el establecimiento y acuerdo respecto al nivel de servicio que se debe realizar.</td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:auto; width:80%">3. El valor del servicio de mantenimiento preventivo para el mes de '.$mesAnterior.' '.$anoAnterior.' es de: CLF '.$total.' </td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td style="height:15px; width:80%" ></td>
	</tr>
	<tr>
		<td colspan=2 width="20%"><p><FONT SIZE=3>III INDICADORES DE CUMPLIMIENTO</FONT></p></td>
		<!--<td style="height:auto; width:80%" ></td>-->
	</tr>
	<tr>
		<td width="20%"></td>
		<td width="80%">1.	Bono: No se cumplió con las condiciones pactadas para la obtención de bono.</td>
	</tr>
	<tr>
		<td width="20%"></td>
		<td width="80%">2.	Penalidad: No corresponde cursar penalidad.</td>
	</tr>
</table>
<p>Por lo anterior, se procede a Otorgar la Aceptación Conforme del Contrato Integrador de Mantenimiento '.$contrato.'.</p>
<table border="0px" width="100%" style="display:$estadoVal;">
	<tr>
		<td width="50%" height="40px" align="center"><img src="/var/www/siompagos.movistar.cl/html/rest/templates/img/logo_telefonica2.jpg" /></td>
		<td width="50%" height="40px" align="center"><img src="/var/www/siompagos.movistar.cl/html/rest/templates/img/logo_telefonica2.jpg" /></td>
	</tr>
	<tr>
		<td width="50%" align="center">p.'.$empresa.' </td>
		<td width="50%" align="center">'.$nomUsuario.'</td>
	</tr>
	<tr>
		<td width="50%" align="center"></td>
		<td width="50%" align="center">Telefónica Chile</td>
	</tr>
</table>';
}
?>
