<?php

//GENERALES ________________________________________________________________________

Flight::route('GET|POST /core/@tabla/*', function($tabla){
    $tablas = Flight::get('core_tables');
    if( array_key_exists ( $tabla , $tablas ) ){
        return(true);
    }	
    Flight::notFound();
});

Flight::route('GET|POST /core/@tabla/list(/@page:[0-9]+)', function($tabla,$page)
{
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array($tabla), $filtros_ini);
    
    $query = "SELECT SQL_CALC_FOUND_ROWS * FROM ".$tabla." WHERE ".$filtros." ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page));
    
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);   
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if(!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total']/$results_by_page);
    }
    Flight::json($res);	
});

Flight::route('GET|POST /core/@tabla/add', function($tabla)
{
    //session_start();
    $tablas = Flight::get('core_tables'); 
    $data = array_merge($_GET,$_POST);
    
    if( $tablas[$tabla]['historial'] ){
        $data['usua_creador'] = $_SESSION['user_id'];
        $data[$tablas[$tabla]['prefijo'].'_fecha_creacion'] = "NOW()";
    }
    $query = "INSERT INTO ".$tabla." ".Flight::dataToInsertString($data);
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET|POST /core/@tabla/upd/@id:[0-9]+', function($tabla,$id){
    $tablas = Flight::get('core_tables'); 
    $data = array_merge($_GET,$_POST);
    $query = "UPDATE $tabla SET ".Flight::dataToUpdateString($data)." WHERE ".$tablas[$tabla]['prefijo']."_id =".$id;
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

Flight::route('GET /core/@tabla/del/@id:[0-9]+', function($tabla,$id){
    $tablas = Flight::get('core_tables'); 
    global $DB_NAME;
    $dbo = new MySQL_Database(); 
    
    //Buscamos si la tabla tiene historial
    $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='$DB_NAME' AND TABLE_NAME='$tabla' AND COLUMN_NAME = '".$tablas[$tabla]['prefijo']."_estado'";
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0){ return $res; };
    
    if( isset($res['data'][0]) ){
        $query = "UPDATE $tabla SET ".$tablas[$tabla]['prefijo']."_estado='NOACTIVO' WHERE ".$tablas[$tabla]['prefijo']."_id =".$id;
    }
    else{
        $query = "DELETE FROM $tabla WHERE ".$tablas[$tabla]['prefijo']."_id=$id";
    }

    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);    
});

Flight::route('GET /core/@tabla/get/@id', function($tabla,$id){
    $tablas = Flight::get('core_tables'); 
    $query = "SELECT * FROM $tabla WHERE ".$tablas[$tabla]['prefijo']."_id =".$id;
    $dbo = new MySQL_Database();
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});



Flight::route('GET|POST /core/@tabla/@id:[0-9]+/@subtabla:[a-z_]+/*', function($tabla, $id, $subtabla) {
    $tablas = Flight::get('core_tables');
    if (!array_key_exists($subtabla, $tablas)) {
        Flight::notFound();
    }

    $dbo = new MySQL_Database();
    $query = "SELECT ".$tablas[$tabla]['prefijo']."_id FROM $tabla WHERE ".$tablas[$tabla]['prefijo']."_id =".$id;
    $res = $dbo->ExecuteQuery($query);
    if($res['rows']==0 ){ Flight::json(array("status"=>0, "error"=>"El ".$tabla." id ".$id." no existe"));}
	
    $campos = "st.*";
    if( $subtabla=="empresa" ) $campos = $campos.", rcst.coem_tipo";
    if( $tabla=="contrato" && $subtabla=="emplazamiento" ) $campos= $campos.", rcst.rece_mpp";	
    if( $tabla=="usuario" && $subtabla=="curso" ) $campos= $campos.", rcst.* ";	
    Flight::set('campos', $campos); 
    return true;
});

/*
Flight::route('GET|POST /core/@tabla/@id:[0-9]+/@subtabla:[a-z_]+/list(/@page:[0-9]+)', function($tabla, $id, $subtabla, $page){
    $tablas = Flight::get('core_tables');
    $campos = Flight::get('campos');
    $results_by_page = Flight::get('results_by_page');
    $filtros_ini = array_merge($_GET,$_POST);
    $filtros = Flight::filtersToWhereString( array($tabla, $subtabla), $filtros_ini);
    
    $dbo = new MySQL_Database();	
    $query = "SELECT $campos 
              FROM ".$tabla." t, ".$subtabla." st, rel_".$tabla."_".$subtabla." rcst"." 
              WHERE 
                    t.".$tablas[$tabla]['prefijo']."_id = ".$id."  
                    AND rcst.".$tablas[$tabla]['prefijo']."_id = t.".$tablas[$tabla]['prefijo']."_id 
                    AND st.".$tablas[$subtabla]['prefijo']."_id = rcst.".$tablas[$subtabla]['prefijo']."_id 
                    AND $filtros ".((is_null($page))?"":" LIMIT ".$results_by_page." OFFSET ".(($page-1)*$results_by_page))
            ;
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0) Flight::json(array("status" => 0, "error" => $res['error']));
    
    $res_count = $dbo->ExecuteQuery("SELECT FOUND_ROWS() as total");
    if ($res_count['status'] == 0) Flight::json(array("status" => 0, "error" => $res_count['error']));
    $res['total'] = intval($res_count['data'][0]['total']);
    
    if (!is_null($page)) {
        $res['pagina'] = intval($page);
        $res['paginas'] = ceil($res['total'] / $results_by_page);
    }
    Flight::json($res);
});
*/
Flight::route('GET|POST /core/@tabla/@id:[0-9]+/@subtabla:[a-z_]+/add/@ids:[0-9]+', function($tabla, $id, $subtabla, $ids){
    $tablas = Flight::get('core_tables'); 
    $tabla_rel = "rel_".$tabla."_".$subtabla;
    $data = array_merge($_GET,$_POST,array($tablas[$tabla]['prefijo']."_id"=>$id, $tablas[$subtabla]['prefijo']."_id"=>$ids));
    
    if( isset($tablas[$tabla_rel]['historial']) && $tablas[$tabla_rel]['historial']){
        $data['usua_creador'] = $_SESSION['user_id'];
        $data[$tablas[$tabla_rel]['prefijo'].'_fecha_creacion'] = "NOW()";
    }
    
    $query = "INSERT INTO $tabla_rel ".Flight::dataToInsertString($data);
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    
    if($res['status']==0){        
        global $DB_NAME;
    
        //Buscamos el prefijo de la tabla
        $query = "SELECT SUBSTRING(COLUMN_NAME,1,LOCATE('_',COLUMN_NAME)-1) as prefijo
                    FROM INFORMATION_SCHEMA.COLUMNS 
                    WHERE TABLE_SCHEMA='$DB_NAME' AND  TABLE_NAME='$tabla_rel' AND COLUMN_NAME LIKE '%_estado' GROUP BY COLUMN_NAME";
        $res = $dbo->ExecuteQuery($query);
        if ($res['status'] == 0) { return $res; };

        if (isset($res['data'][0])) {
            $data[$res['data'][0]['prefijo'] . '_estado'] = "ACTIVO";
        }
        
        $query = "UPDATE $tabla_rel SET " . Flight::dataToUpdateString($data) . " WHERE " . $tablas[$tabla]['prefijo'] . "_id=$id AND " . $tablas[$subtabla]['prefijo'] . "_id=$ids";
        $res = $dbo->ExecuteQuery($query);
    }
    
    Flight::json($res);
});

Flight::route('GET /core/@tabla/@id:[0-9]+/@subtabla:[a-z_]+/del/@ids:[0-9]+', function($tabla, $id, $subtabla, $ids){
    $tablas = Flight::get('core_tables'); 
    $tabla_rel = "rel_".$tabla."_".$subtabla;
    global $DB_NAME;
    
    //Buscamos el prefijo de la tabla
    $query = "SELECT  SUBSTRING(COLUMN_NAME,1,LOCATE('_',COLUMN_NAME)-1) as prefijo
              FROM INFORMATION_SCHEMA.COLUMNS 
              WHERE TABLE_SCHEMA='$DB_NAME' AND  TABLE_NAME='$tabla_rel' AND COLUMN_NAME LIKE '%_estado' GROUP BY COLUMN_NAME";
    $dbo = new MySQL_Database();    
    $res = $dbo->ExecuteQuery($query);
    if ($res['status'] == 0){ return $res; };
    
    if( isset($res['data'][0]) ){
        $prefijo = $res['data'][0]['prefijo'];
        $query = "UPDATE $tabla_rel SET ".$prefijo."_estado='NOACTIVO' WHERE ".$tablas[$tabla]['prefijo']."_id=$id AND ".$tablas[$subtabla]['prefijo']."_id=$ids";
    }
    else{
        $query = "DELETE FROM $tabla_rel WHERE ".$tablas[$tabla]['prefijo']."_id=$id AND ".$tablas[$subtabla]['prefijo']."_id=$ids";
    }
    
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);

});

Flight::route('GET|POST /core/@tabla/@id:[0-9]+/@subtabla:[a-z_]+/upd/@ids:[0-9]+', function($tabla, $id, $subtabla, $ids){
    $tablas = Flight::get('core_tables'); 
    $tabla_rel = "rel_".$tabla."_".$subtabla;
    $data = array_merge($_GET,$_POST);
    
    $dbo = new MySQL_Database();   
    $query = "UPDATE $tabla_rel SET " . Flight::dataToUpdateString($data) . " WHERE ".$tablas[$tabla]['prefijo']."_id=$id AND ".$tablas[$subtabla]['prefijo']."_id=$ids";
    $res = $dbo->ExecuteQuery($query);
    Flight::json($res);
});

?>