
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<table border="0px" width ="100%" style="/*margin-left: 5px; */margin-top:0px;">
   <tr>
      <td width ="15%" valign="TOP">
         <form class="form-horizontal siom-form-tiny" role="form" action="#/pago/costeo/ver/filtro/ot" method="POST" id="pago-costeo-ver-filtro">
            <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:0px;">
            <legend>Seleccione Filtros</legend>
            <table border="0px" width ="100%" ">
                <tr>
                  <td width="40%">
                     <button type="button" class="btn btn-primary  btn-xs btn-margen" id="Limpiarfiltro">Limpiar</button>
                  </td>
                 
                  <td width="60%">
                     <button type="button"  class="btn btn-primary  btn-xs btn-margen" id="BuscarResumenPago" data-tipo="OS">Buscar</button>
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <hr>
                  </td>
               </tr>
               <tr>
                  <td align="right">
                    <label for="periodo" title='Periodo'  class="control-label">Periodo</label>
                  </td>
                  <td class="control-label-th">
                    <input type="text" id="periodo" class="form-control" name="periodo" onkeypress="return validaNumericos(event)" maxlength="6" size="8" value="{{periodo}}" />
                  </td>
               </tr>
               <tr  >
                  <td colspan="2" height="10" >
                  </td>
               </tr>
               <tr >
                  <td align="right">
                    <label for="id_ot" title='Actividad' class="control-label">Actividad</label>
                  </td>
                  <td class="control-label-th">
                  	<select class="botones text-ancho" data-width="100%" selected-value="{{actividad}}" id="tipo_ot" name ="tipo_ot">
	                           		<option {{copa_tipo_ot_select}} value="OS"> OS-Correctivo</option>
									         <option {{copa_tipo_ot_select}} value="LMT"> OS-L.Media Atención</option>
	                           		<option {{copa_tipo_ot_select}} value="MNT"> MNT-Mantenimiento</option>
	               	</select>
                  </td>
               </tr>
               <tr>
                  <td  colspan="2" height="10">
                  </td>
               </tr>
            </table>
         </fieldset>
         </form>
      </td>
      <td width ="85%">
         <div class="col-md-12" id="pago-costeo-lista" style="border: 0px solid #990000;"></div>
      </td>
   </tr>
</table>
<script src="js/pago_costeo_ver_filtro.js" type="text/javascript" charset="utf-8"></script>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>