<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>    
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>

<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/pago_presupuesto/add" method="POST" id="form-pago_presupuesto">
	<div class="col-md-12">
		<div class="row" style="/*margin-left: 5px; */margin-top:20px;">
			<div class="col-md-4">
				<fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:10px; shape-margin:10px;  ">
				<legend>Ingreso Linea Presupuestaria</legend>
				<div class="col-md-12">
					<div class="col-md-12" style="float: left; padding-top: 0px;padding-right: 0px;padding-left: 0px;">
						<!--<div class="form-group" style="display:block">
							<label  for="pago_linea" title='pago_linea' class="col-sm-4 control-label">Linea</label>
							<div class="col-md-6">
								<input type="text" id="papr_linea" name="pago_linea" placeholder="Linea" value maxlength="20" />
							</div>
					</div>-->
					<div class="form-group" style="display:block">
						<label for="pago_empresa" title='empresa'  class="col-sm-4 control-label">Empresa</label>
						<div class="col-md-6">
							<select class="selectpicker" data-width="100%"  id="papr_empresa" name="pago_empresa">
							{{#empresa}}
								<option value="{{soci_nombre}}">{{soci_nombre}}</option>
							{{/empresa}}
							</select>
						</div>
					</div>
					<br /> 
					<div class="form-group" style="display:block">
						<label for="pago_sociedad" title='sociedad'  class="col-sm-4 control-label">Sociedad</label>
						<div class="col-md-6">
							<select class="selectpicker" data-width="100%" id="papr_sociedad" name="pago_sociedad">
							{{#sociedad}}
								<option value="{{soci_codigo}}">{{soci_codigo}}</option>
							{{/sociedad}}
							</select>
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_capex_opex" title='capex_opex' class="col-sm-4 control-label">ID_Opex / Capex</label>
						<div class="col-md-6">
                           <input type="text" id="papr_capex_opex" name="pago_capex_opex" placeholder="ID_Opex / Capex" maxlength="20"/>
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_servicio" title='pago_servicio' class="col-sm-4 control-label">Nombre Servicio</label>
						<div class="col-md-6">
                            <textarea class="form-control" type="text" name="pago_servicio"  id="papr_servicio" maxlength="150"></textarea>
                          <!-- <input type="text" id="papr_servicio" name="pago_servicio" placeholder="Servicio" value maxlength="150" />-->
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_proveedor" title='pago_proveedor' class="col-sm-4 control-label">Proveedor</label>
						<div class="col-md-6">
							<select class="selectpicker" data-width="100%"  id="papr_proveedor" name="pago_proveedor">
							{{#proveedor}}
								<option value="{{empr_nombre}}">{{empr_nombre}}</option>
							{{/proveedor}}
							</select>
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_anio" title='pago_anio' class="col-sm-4 control-label">A�o</label>
						<div class="col-md-6">
							<input type="text" id="papr_anio" onkeypress="return validaNumericos(event)" name="pago_anio" placeholder="{{anio_actual}}" maxlength="4" />
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_monto" title='pago_monto' class="col-sm-4 control-label">Monto CLP</label>
						<div class="col-md-6">
							<input type="text" id="papr_monto" name="pago_monto" onkeypress="return validaNumericos(event)" placeholder="Monto" maxlength="20" />
						</div>
					</div>
					<br />
					<div class="form-group" style="display:block">
						<label for="pago_tipo_linea" title='pago_tipo_linea' class="col-sm-4 control-label">Tipo Linea</label>
						<div class="col-md-6">
							<select class="selectpicker" data-width="100%"  id="tipo_linea" name="tipo_linea">
							{{#tipo_linea}}
								<option value="{{.}}">{{.}}</option>
							{{/tipo_linea}}
							</select>
						</div>
					</div>
					<div class="col-md-13">
						<br>
						<button type="button" class="btn btn-primary btn-default pull-right" id="id_papr_boton_consultar" data-tipo="MNT"> Ingresar</button>
					</div>
				</div>
			</div>
            </fieldset>
        	<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/presupuesto_tabla" method="POST" id="form-pago-presupuesto-tabla">
        		</div>
            		<div class="col-md-8" id="pago-presupuesto-tabla"></div>
        		</div>
    		</form>
        </div>
    </div>
</div>
</form>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_presupuesto.js" type="text/javascript" charset="utf-8"></script>