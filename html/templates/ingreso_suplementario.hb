<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_pago.js" type="text/javascript" charset="utf-8"></script> 
<form class="form-horizontal siom-form-tiny" role="form" action="#/pago/ingreso_suplementario/add" method="POST" id="form_ingreso_suplemnetario">
   <div class="col-md-9">
      <div class="row" style="/*margin-left: 5px; */margin-top:20px;">
         <fieldset class="siom-fieldset" id="siom-usuario-info">
            <legend>Ingreso Suplementario</legend>
            <table colspan="4" border= "1px solid black" width="90%">
               <tr> 
                     <td width="15%" > 
                        <label for="insu_anio" title='Año de ingreso suplemnto' class="col-sm-4 control-label  pull-right ">Año</label>
                     </td >
                     <td width="30%" > 
                        <input type="text" id="insu_anio" name="insu_anio" class="form-insu_anio" placeholder="Año">
                     </td>
               </tr>
               <tr> 
                  <td width="15%" > 
                     <label for="insu_monto" title='Monto a inyectar/quitar al presupuesto ' class="col-sm-4 control-label pull-right ">Monto</label>
                  </td>
                  <td> 
                     <input type="text" id="insu__monto" name="insu_monto" class="form-insu_monto" placeholder="Monto">
                  </td>
                  <td width="15%"> 
                     <label for="insu_moneda"  title='Ingresar el tipo de moneda correspondiente al presupuesto ' class="pull-right ">Tipo Moneda</label>
                  </td>
                  <td>  
                     <select id="insu_moneda" class="selectpicker" value="{{insu_moneda}}" data-width="100%" name="insu_moneda" title="Moneda"   data-container="body" >
                        <option value="CLP">CLP</option>
                        <option value="UF">UF</option>
                     </select>
                  </td>
               </tr>
               <tr> 
                     <td> 
                        <label for="insu_moneda"  title='Ingresar el tipo de gasto operacional ' class="pull-right ">Gasto OP</label> 
                     </td>
                     <td colspan="3">  
                        <select id="insu_gp" class="selectpicker" value="{{insu_gp}}" data-width="100%" name="insu_gp" title="gastoOP"   data-container="body" >
                        <option value="CAPEX">CAPEX</option>
                        <option value="OPEX">OPEX</option> 
                     </td>
               </tr>
               <tr >
                  <td>
                     <label for="insu_observacion"  title='Ingresar la boservacion correspondiente '>Observación</label> 
                  </td>
                  <td colspan="3">
                      <textarea class="col-sm-4 siom-value" id="insu_observacion" placeholder="Observación"></textarea>
                  </td>
               </tr>
            </table>
        <!--     <table border= "1px solid black" width="90%">
               <tr border='1'>
                   <td border='1'>
                        <label for="insuanio" title='Año del contrato solicitado' class="col-sm-4 control-label">Monto</label>
                  </td border='1'>
                  <td colspan="2"  >
                     <div class="form-group" style="display:block">
                        <div class="col-md-6">
                           <input type="text" id="insu_monto" name="insu_monto" class="form-insu_monto" placeholder="Monto">
                        </div>
                     </div>
                  </td>
                  <td colspan="2">
                     <div class="form-group" style="display:block">
                        <label for="papreanio" title='Año del contrato solicitado' class="col-sm-4 control-label">Monto</label>
                        <div class="col-md-6">
                           <input type="text" id="insu__monto" name="insu_monto" class="form-insu_monto" placeholder="Monto">
                             <select id="insu_tipo_monto" class="selectpicker" value="{{insu__tipo_monto}}" data-width="100%" name="insu_tipo_monto" title="insu_tipo_monto"   data-container="body" >
                              <option value="CAPEX">CAPEX</option>
                              <option value="OPEX">OPEX</option>
                           </select>
                        </div>
                     </div>
                  </td>
               </tr>
               <tr border='1' >
                  <td colspan="2">
                     <div class="form-group">
                           <label for="insu_moneda"  title='Ingresar el tipo de moneda correspondiente al presupuesto ' class="col-md-4 control-label">Tipo Moneda</label>
                        <div class="col-md-5">
                           <select id="insu_moneda" class="selectpicker" value="{{insu_moneda}}" data-width="100%" name="insu_moneda" title="Moneda"   data-container="body" >
                              <option value="CLP">CLP</option>
                              <option value="UF">UF</option>
                           </select>
                        </div>
                     </div>
                  </td>
                   <td colspan="2">
                     <div class="form-group">
                     <label for="insu_observacion" class="col-sm-2 siom-label">Observación</label>
                     <textarea class="col-sm-2 siom-value" id="insu_observacion" placeholder="Observación"></textarea>
                  </div>
                        </div>
                     </div>
                  </td>
               </tr>
            </table>
            <div class="col-md-12">
               <button type="submit" class="btn btn-primary btn-default pull-right" id="id_BotonConsultar">Ingresar</button>
            </div> -->
         </fieldset>
      </div>
   </div>
</form>
 