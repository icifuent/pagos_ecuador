<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<form role="form" action="#/pago/detalle/filtro" method="POST" id="siom-form-costeo-generar">
	<!--form role="form" action="#/pago/generar/costeo/actividad/filtro" method="POST" id ="siom-form-costeo-generar" -->
	<div class="col-md-12">
		<fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
			<legend>Detalle Costeo actividad </legend>
			<table border="0px" width="100%">
				<tr>

					<td width="5%" align="right">
						<label>Periodo</label>
					</td>
					<td width="5%" class="control-label-th">
						<input type="text" class="text-left" onkeypress="return validaNumericos(event)" readonly maxlength="6" id="periodo" name="periodo"
						 value="{{periodo}}" />
					</td>
					<td width="5%" align="right">
						<label for="actividad" title='Actividad'>Actividad</label>
					</td>
					<td width="10%" class="control-label-th">
						<input type="text" class="text-left" readonly id="actividad" name="actividad" value="{{actividad}}" />
						<!--select class="selectpicker" data-width="100%"  id="actividad" name="actividad">
						   <option value="OS"> Orden Servicio </option>
						   <option value="MNT"> Mantenimiento </option>
						</select-->
					</td>
					<td width="10%">
						<!--button type="button" class="btn btn-primary btn-default pull-right" id="buscarFiltro">Buscar</button-->
					</td>
					<td width="60%"></td>
					<td width="10%">
						<div border="0px">
							<a type="button" style='width:120px;' class="btn btn-primary btn-default pull-right btn-xs" id="id_papre_boton_regresar"
							 data-tipo="Regresar" href="#/pago/regresar/resumen">Regresar</a>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
</form>
<form role="form" action="#/pago/costeo/actividad/add" method="POST" id="siom-form-costeo" name="siom-form-costeo">
	<div class="col-md-12">
		<fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
			<legend>Registros Pendientes de Costeo</legend>
			<table border="0px" width="100%">
				<tr>
					<td width="40%">
						<input type="text" id="str_id_ot" name="str_id_ot" style="display:none;" />
						<input type="text" id="str_check_penalidad_value" name="str_check_penalidad_value" style="display:none;" />
						<input type="text" id="str_monto_total_cabecera" name="str_monto_total_cabecera" style="display:none;" />
						<input type="text" id="str_monto_total" name="str_monto_total" style="display:none;" />
						<input type="text" id="copa_id" name="copa_id" value={{copa_id}} style="display:none;" />
					</td>
					<input type="text" id="str_cantidad_lpu" name="str_cantidad_lpu" style="display:none;" />
					</td>

					<td width="20%" align="right" style="display: none;">
						<label for="linea" title='linea'>Linea presupuestaria opex</label>
					</td>
					<td width="30%" style="display: none;">
						<select class="selectpicker" data-width="100%" id="lineaopex" name="lineaopex">
							<option value="0"> --Seleccione Linea-- </option>
							{{#linea_presupuestoopex}}
							<option value="{{papr_id}}" {{papr_id_seleccionada}}>{{papr_nombre_servicio}}</option>
							{{/linea_presupuestoopex}}
						</select>
					</td>
					<td width="10%"></td>
				</tr>
				<br>
				<tr>
					<td width="40%"></td>
					<td width="20%" align="right" style="display: none;">
						<label for="linea" title='linea'>Linea presupuestaria capex</label>
					</td>
					<td width="30%" style="display: none;">
						<select class="selectpicker" data-width="100%" id="lineacapex" name="lineacapex">
							<option value="0"> --Seleccione Linea-- </option>
							{{#linea_presupuestocapex}}
							<option value="{{papr_id}}" {{papr_id_seleccionada}}>{{papr_nombre_servicio}}</option>
							{{/linea_presupuestocapex}}
						</select>
					</td>
					<td width="10%">
						<div border="0px">
							<button type="button" style='width:120px;' class="btn btn-primary btn-default pull-right btn-xs" id="GuardarCosteo" data-saving-text="Guardando..."
							 data-processing-text="Guardando...">Guardar Costeo</button>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="4" width="100%">
						<table width="100%" border="0px" data-toggle="table" data-sort-order="desc" cclass="panel panel-default" id="lista_os_periodo">
							<thead class="form-group">
								<tr class="panel panel-default">
									<th class=" control-label-th" data-sortable="true">NÚMERO
									</th>
									<th class=" control-label-th" data-sortable="true">ITEM LPU</th>
									<th style="display:none;">
										<input type="checkbox" disabled="true" id="check_capex_all" name="check_capex_all"> CAPEX
									</th>
									<th style="display:none;">
										<input type="checkbox" disabled="true" id="check_opex_all" name="check_opex_all"> OPEX
									</th>
									<th class=" control-label-th" data-sortable="true">CANTIDAD
									</th>
									<th class=" control-label-th" data-sortable="true">PRECIO
									</th>
									<th class=" control-label-th" data-sortable="true">MONTO TOTAL
									</th>
									<th class=" control-label-th">
										<input type="checkbox" id="check_penalidad_all" name="check_penalidad_all"> PENALIDAD
									</th>
									<th style="display:none;" data-sortable="true">TIPO MONEDA
									</th>
									<th style="display:none;" data-sortable="true">F. APROBACIÓN SIOM
									</th>
								</tr>
								<tr>
									<th class=" control-label-th" data-sortable="true">
										<input type="text" id="periodo" name="periodo" value="{{periodo}}" style="display:none;" />
										<input type="text" id="actividad" name="actividad" value="{{actividad}}" style="display:none;" />
									</th>
									<th class=" control-label-th" data-sortable="true"></th>

									<th style="display:none;">
										<input class="text-rigth" id="monto_total_capex" name="monto_total_capex" readonly value="{{copa_monto_capex}}">
									</th>
									<th style="display:none;">
										<input class="text-rigth" id="monto_total_opex" name="monto_total_opex" readonly value="{{copa_monto_opex}}">
									</th>
									<th class=" control-label-th" data-sortable="true">
										<input class="text-rigth" id="monto_total_cabecera" name="monto_total_cabecera" readonly value="{{parsePeso copa_monto_total}}">
									</th>
									<th class=" control-label-th">
									</th>

									<th class=" control-label-th" data-sortable="true"></th>
									<th class=" control-label-th" data-sortable="true"></th>

								</tr>

							</thead>
							<tbody class="form-group cambio">
								{{#pago_os_periodo}}
								<tr class="panel panel-default">
									<td width="5%">{{orse_id}}
										<input type="text" id="id_ot" name="id_ot" value="{{id_ot}}" style="display:none;" />
									</td>
									<td width="40%" class="text-left">{{actividad_ot}}
										<input type="hidden" id="actividad_ot" name="actividad_ot" value="{{actividad_ot}}" />
									</td>
									<td width="14px" id="capex" style="display:none;">
										<input type="checkbox" disabled="true" id="check_capex" name="check_capex" value="{{id_ot}}" {{checkedCapex}} onclick="checkPagarCapex({{id_ot}})"
										/> {{capex_ot}}
										<input type="hidden" id="capex_ot" name="capex_ot" value="{{capex_ot}}" />
										<input type="hidden" id="check_capex_value" name="check_capex_value" value="NO" />
									</td>
									<td width="14px" id="opex" style="display:none;">
										<input type="checkbox" disabled="true" id="check_opex" name="check_opex" value="{{id_ot}}" {{checkedOpex}} onclick="checkPagarOpex({{id_ot}})"
										/> {{opex_ot}}
										<input type="hidden" id="opex_ot" name="opex_ot" value="{{opex_ot}}" />
										<input type="hidden" id="check_opex_value" name="check_opex_value" value="NO" />
									</td>
									<td width="10%" class=" control-label-th">
										<input readonly id="cantidad_lpu_{{id_ot}}" onkeypress="return validaNumericos(event)" name="cantidad_lpu" id="cantidad_lpu"
										 class="text-rigth cantidad_lpu" value="{{parsePeso cantidad_lpu}}">

									</td>
									<td width="10%" class=" control-label-th">
										<input readonly="readonly" id="precio_lpu_{{id_ot}}" name="precio_lpu" class="text-rigth" value="{{parsePeso precio_lpu}}">
									</td>
									<td width="10%" class=" control-label-th">
										<input readonly="readonly" id="monto_total_{{id_ot}}" name="monto_total" class="text-rigth" value="{{parsePeso monto_total_capex_opex}}">
									</td>
									<td width="10%" class=" control-label-th">
										<input type="checkbox" id="check_penalidad" name="check_penalidad" value="{{id_ot}}" {{checkedPenalidad}} onclick="checkPenalidad({{id_ot}})"
										/>
										<input hidden id="check_penalidad_value" name="check_penalidad_value" value="NO" />
									</td>
									<td width="14px" id="tipo_moneda" style="display:none;">{{tipo_moneda}}
										<input type="hidden" id="tipo_moneda" name="tipo_moneda" value="{{tipo_moneda}}" />
									</td>
									<td width="14px" style="display:none;">{{fecha_aprobacion_ot}}
										<input type="hidden" id="fecha_aprobacion_ot" name="fecha_aprobacion_ot" value="{{fecha_aprobacion_ot}}" />
									</td>
								</tr>
								{{/pago_os_periodo}}
							</tbody>
						</table>
						<div class="panel-footer siom-paginacion">
							<div class="row">
								<div class="col-md-8 text-right">
									<div class="pagination-info">Página {{pagina}}/{{paginas}} ({{total}} registros)</div>
									<div id="pagination" class="pagination-buttons" data-page="{{pagina}}" data-total="{{paginas}}" data-actividad="{{actividad}}"
									 data-periodo="{{periodo}}" data-max-visible="6" data-filters="{{JSON filtros}}"></div>
								</div>
							</div>
						</div>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
</form>
<style>
	.fixed-table-container thead th .th-inner,
	.fixed-table-container tbody td .th-inner {
		line-height: 15px;
		text-align: center;
	}
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_generar_costeo_actividad.js" type="text/javascript" charset="utf-8"></script>