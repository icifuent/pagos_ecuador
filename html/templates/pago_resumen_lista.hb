
<link href="css/bootstrap.css" rel="stylesheet" media="screen">
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<link href="css/content.css" rel="stylesheet" media="screen">
<form class="form-horizontal siom-form-tiny" role="form"  action="#/pago/resumen/lista" method="POST" id="FormPagoResumenLista">
   <!-- fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:0px; shape-margin:0px;  "-->
      <h5>Resumen Costeo</h5>
      <table id="tabla" border="1px" width="100%"  class="panel panel-default" >
         <thead class="form-group">
            <tr class="panel panel-default" >
               <th width="5%" class=" control-label-th"><small>N.COSTEO</small></th>
               <th width="5%" class=" control-label-th"><small>PERIODO </small></th>
               <th width="5%" class=" control-label-th"><small>TIPO    </small></th>
               <th width="11%"class=" control-label-th"><small>ESTADO  </small></th>
               <th width="5%" class=" control-label-th"><small>CANTIDAD</small></th>
               <th width="7%" class=" control-label-th"><small>M.TOTAL	</small></th>
               <th width="7%" class=" control-label-th"><small>CAPEX 	</small></th>
               <th width="7%" class=" control-label-th"><small>OPEX		</small></th>
               <th width="3%" class=" control-label-th"><small>P.PAGO</small></th>
              <!--  <th width="7%" class=" control-label-th"><small>DERIVADA</small></th>
                 <th></th>
               <th width="7%" class=" control-label-th"><small>HEM 		</small></th> -->
             

               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Linea Presupuestaria Capex</th-->
               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Linea Presupuestaria Opex/th-->
               <!--th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo Linea</th-->
             <!--   <th width="7%" class=" control-label-th"><small>TIPO CAMBIO</small>  </th> -->
               <th width="9%" class=" control-label-th"><small>OBSERVACIÓN</small>  </th>
               <th width="7%" class=" control-label-th">			                       </th>
               <th width="7%" class=" control-label-th">                            </th>
            </tr>
         </thead>
         <tbody  class="form-group cambio">
            {{#monto}}
            <tr class="panel panel-default cambio" >
               <td   width="4%" class="text-center" >
                 <a  href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}/{{copa_id}}" <small>{{copa_id}}</small></a>
                  <input id="copa_id" name="copa_id"  value="{{copa_id}}" type="hidden"/>
               </td>
               <td width="8%" class="text-center" >
                  <input style='width:50px;'class="text-rigth"  type="text" id="periodo" name="periodo"  onkeypress="return validaNumericos(event)" placeholder="{{copa_periodo}}" value="{{copa_periodo}}" maxlength="6" />
                  <!--a href="#/pago/costeo/periodo/{{copa_periodo}}/actividad/{{copa_tipo_ot}}/{{copa_id}}" class="click">{{copa_periodo}}</a-->
                  <!--input id="copa_id" name="copa_id"  value="{{copa_id}}" type="hidden"/-->
               </td>
               <td width="10%" id="tipo_ot"class=" control-label-th">        
                    <small style="padding-right: 3px;">{{copa_tipo_ot}}</small>
                    <input type="text" id="actividad_valor" name="actividad_valor"  value = {{copa_tipo_ot}} hidden /> 
               </td>
               <td width="10%"id="estado" class="text-left ">
                    <small style="padding-right: 3px;">{{copa_estado}}</small>
                     <input type="text" id="copa_estado" name="copa_estado"  value = {{copa_estado}} hidden />
                </td>
               <td width="7%" id="cantidad_total" class="control-label"> 
                    <small style="padding-right: 3px;">{{copa_cantidad_total}}</small>
                </td>
               <td width="8%" id="monto_total"  class="control-label">    
                    <small style="padding-right: 3px;">{{parsePeso copa_monto_total}}</small>
                </td>
               <td Width="8%" id="monto_capex" class="control-label">    
                    <small style="padding-right: 3px;" >{{parsePeso copa_monto_capex}}</small>
                </td>
               <td Width="8%" id="monto_opex" class="control-label">     
                    <small style="padding-right: 3px;">{{parsePeso copa_monto_opex}}</small>
                </td >
                  <td style="padding-left: 10px;"> 
                    <a id="botonEditarhemyderivada" href="#/pago/ingresar/hemyderivada/copa_id/{{copa_id}}" class="glyphicon glyphicon-pushpin" style="display:{{copa_visibilidad_editar_hemyderivada}}"></a>
                </td>

          <!-- 
               <td Width="8%" class="text-center" >
                  <input type="text" id="derivada" style='width:75px;'class="text-left" name="derivada"  onkeypress="return validaNumericosSinComa(event)" placeholder="{{copa_derivada}}" value="{{copa_derivada}}" maxlength="20" />
                  
               </td>
              
               <td Width="8%" >
                  <input type="text" id="hem" name="hem" class="text-left"  style='width:75px;' onkeypress="return validaNumericosSinComa(event)" placeholder="{{copa_hem}}" value="{{copa_hem}}" maxlength="20" />
                </td>
               -->

               <!--td class="col-md-3">{{papr_nombre_servicio_capex}}
                  <input id="papr_id" name="papr_id_capex"  value="{{papr_id_capex}}" type="hidden"/>
                  </td>
                  <td class="col-md-3">{{papr_nombre_servicio_opex}}
                  <input id="papr_id" name="papr_id_opex"  value="{{papr_id_opex}}" type="hidden"/>
                  </td-->
               <!--td class="col-md-3">{{papr_tipo}}
                  <input id="papr_tipo" name="papr_tipo"  value="{{papr_tipo}}" type="hidden"/>
                  </td-->
            <!--    <td Width="8%" >
                  <input type="text" style='width:75px;' class="text-rigth"  onkeypress='return validaNumericos(this.value,event)'  id="tipo_cambio" name="tipo_cambio"  maxlength="20" placeholder="{{parsePeso copa_tipo_cambio}}" value="{{parsePeso copa_tipo_cambio}}" />
               </td> -->
               <td width="15%"  style="padding-left: 30px;">
                  <input type="text" maxlength="50" id="observacion_resumen"  class="text-rigth" name="observacion_resumen" value="{{copa_observacion}}"  placeholder="{{copa_observacion}}"  />
               </td>
               <td width="8%">
                  <button id="guardarResumenCosteo" name="guardarResumenCosteo" type="button" class="btn btn-default" data-copa_id="{{copa_id}}" style="padding:2px;height:20px">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
                  </button>
                  <a id="botonEditar" href="#/pago/editar/costeo/copa_id/{{copa_id}}" class="glyphicon glyphicon-pencil" style="display:{{copa_visibilidad_editar}}"></a>
                  <a id="botonAsignarCosteo" href="#/pago/asignar/usuario/validador/costeo/copa_id/{{copa_id}}" class="glyphicon glyphicon-plus" style="display:{{copa_visibilidad_asignar_validador_costeo}}" ></a>
                  <a id="botonAsignarActa" href="#/pago/asignar/usuario/validador/acta/copa_id/{{copa_id}}" class="glyphicon glyphicon-plus" style="display:{{copa_visibilidad_asignar_validador_acta}}" ></a>
                  <a role="menuitem" id="botonhemyderivadaver{{copa_id}}" onclick="abrirModalhemyderivada({{cont_id}},{{copa_id}},'{{copa_tipo_ot}}');" class="glyphicon glyphicon-search"tabindex="-1" href="#" data-toggle="modal" data-target="#modalMantHistoricos"data-title="Historico Mantenimientos" > 
                         
                       </a>
               </td>
               <td width="10%">
                  <div id="botonera_validar"  style="display:{{copa_validar_acta}}">
                     <button type="button" style='width:100px;' class="btn btn-primary btn-default pull-right btn-xs " id="validaracta" data-periodo="{{periodo}}" data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Acta</button>
                  </div>
                  <div id="botonera_validar"  style="display:{{copa_validar_costeo}}">
                     <button type="button" style='width:100px;' class="btn btn-primary btn-default pull-right btn-xs" id="validarcosteo" data-periodo="{{periodo}}" data-copa-id="{{copa_id}}" data-actividad="{{actividad}}">Validar Costeo</button>
                  </div>
               </td>
            </tr>
            {{/monto}}
         </tbody>
      </table>
      <!-- inicio modal historico -->
<div class="modal fade" id="modalMantHistoricos">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Detalle Costeo.</h4>
            </div>
            <div class="modal-body">
            <div class="row">
              <div class="col-md-12" id="detalle-hemyderivada">
            </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>
<!-- fin modal historico -->
   <!-- /fieldset-->
</form>
<table id="tabla" border="0px" width="100%" >
   <tr>
      <td>
         <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
      </td>
   </tr>
</table>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_resumen_lista.js" type="text/javascript" charset="utf-8"></script>