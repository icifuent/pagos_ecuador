<div class="col-md-12">
    <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
        <legend>Resumen Presupuestosssssssssssss {{anio}}</legend>
        <div class="col-md-12">
            <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
            <table data-toggle="table" data-sort-order="desc">
                <thead>
                    <tr>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Empresa</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Sociedad</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">ID_Opex/Capex</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Nombre Servicio</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Tipo Linea </th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Proveedor</th>
                        <!--<th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Año</th>-->
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">$(CLP)</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Consumido</th>
                        <th colspan="1" class="col-md-2" data-align="right" data-sortable="true">Saldo</th>
						<th colspan="1" class="col-md-2" data-align="right" data-sortable="true"></th>
                    </tr>
                </thead>
                    <tbody> 
                        {{#pago_pre_tabla}}  
                            <tr>
                                <td class="col-md-2" >{{empresa}}</td>
                                <td class="col-md-2" >{{sociedad}}</td>
                                <td class="col-md-2" >{{id_capex_opex}}</td>
                                <td class="col-md-2" >{{nombre_servicio}}</td>
                                <td class="col-md-2" >{{papr_tipo}}</td>
                                <td class="col-md-2" >{{proveedor}}</td>
                                <!-- <td class="col-md-2" hidden >{{anio}}</td>-->
                                <td class="col-md-2" >{{monto}}</td>
                                <td class="col-md-2" ></td>
                                <td class="col-md-2" >{{monto}}</td>
								<td>
									<button id="eliminarRegistroPresupuesto" name="eliminarRegistroPresupuesto" type="button" class="btn btn-default" data-copa_id="{{copa_id}}" style="padding:2px;height:20px;display:none;">
										<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
									</button>
								</td>
                            </tr>    
                        {{/pago_pre_tabla}}                
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
    <div class="col-md-4 text-left">
        <button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}">Descargar en Excel</button>
    </div>

   <!--div class="col-md-12">
      <button type="button" class="btn btn-primary btn-default pull-right" id="GuardarCosteo" data-saving-text="Guardando..."data-processing-text="Guardando..." >Guardar Costeo</button>
   </div-->
</div>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script> 
<script src="js/pago_presupuesto_tabla.js" type="text/javascript" charset="utf-8"></script> 

