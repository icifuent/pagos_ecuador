<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<script src="js/detalle_pago.js" type="text/javascript" charset="utf-8"></script> 
<form class="form-horizontal siom-form-tiny" role="form" action="#/detalle_pago" method="POST" id="form_ingresar_pago">
   <div class="col-md-9">
      <div class="row" style="/*margin-left: 5px; */margin-top:20px;">
         <fieldset class="siom-fieldset" id="siom-usuario-info">
            <legend>Ingreso de Presupuesto</legend>
            <table width="90%">
               <tr border='1'>
                  <td colspan="2"  >
                     <div class="form-group">
                         <label for="empr_alias" title= 'Ingresar el monto correspondiente al presupuesto  adquirido para el contrato ' class="col-md-3 control-label">Monto</label>
                        <div class="col-md-5">
                           <input  class="form-monto" data-width="100%" type="text" id="id_detapag_monto" name="detapag_monto"    placeholder="Monto">
                     </div>
                  </td>
                  <td colspan="2">
                     <div class="form-group">
                        <label for="usua_rut" title='Ingresar el año correspondiente al presupuesto ' class="col-md-4 control-label">Año</label>
                        <div class="col-md-4">
                           <input box-sizing:border-box  class="form-anio" data-width="100%" type="text" id="id_detapag_anio" name="detapag_anio"   placeholder="Año" value maxlength="4">
                        </div>
                     </div>
                  </td>
               </tr>
               <tr border='1' >
                  <td colspan="2">
                     <div class="form-group">
                           <label for="empr_alias"  title='Ingresar el tipo de moneda correspondiente al presupuesto ' class="col-md-4 control-label">Tipo Moneda</label>
                        <div class="col-md-5">
                           <select class="selectpicker" data-width="100%" id="id_detapag_moneda" name="detapag_moneda" title="Contrato">
                              <option>CLP</option>
                              <option>UF</option>
                           </select>
                       
                        </div>
                     </div>
                  </td>
               </tr>
            </table>
            <div class="col-md-12">
               <button type="submit" class="btn btn-primary btn-default pull-right" id="id_BotonConsultar">Ingresar</button>
            </div>
         </fieldset>
      </div>
   </div>
</form>
 