<script src="js/detalle_os_periodo.js" type="text/javascript" charset="utf-8"></script>
<form role="form" action="#/pago/costeo/add" method="POST" id ="siom-form-costeo" >
    <div class="col-md-12">
        <fieldset class="siom-fieldset" style="padding-bottom:10px; margin-top:0px;">
            <legend>Detalle Registros Por Periodo</legend>            
            <label>Periodo</label>
            <input type="text" id="periodo" name="periodo" value="{{periodo}}" />
            <a type="button" class="btn btn-primary btn-default pull-right" id="id_papre_boton_regresar" data-tipo="Regresar" href="#/pago/resumen">Regresar</a>
            <div class="col-md-12" style="float: left; padding-top: 0px;  padding-bottom: 0px;padding-right: 0px;padding-left: 0px;">
                <table data-toggle="table" data-sort-order="desc" class="siom-os-lista siom-os-presupuesto-lista-items" id="lista_os_periodo">
                    <thead>
                        <tr>
                            <th class="col-md-2" data-align="right" data-sortable="true">Número</th>
                            <th class="col-md-2" data-align="right" data-sortable="true">Tipo</th>
                            <th colspan="5" class="col-md-3" data-align="center" data-sortable="true">Monto</th>
                            <th class="col-md-2" data-align="right" data-sortable="true">Tipo Moneda</th>
                            <th class="col-md-2" data-align="right" data-sortable="true">Fecha Aprobación SIOM</th>
                        </tr>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Total</th>
                            <th><input type="checkbox" id="check_capex_all" name="check_capex_all"></th>
                            <th>Capex</th>
                            <th><input type="checkbox" id="check_opex_all" name="check_opex_all"></th>
                            <th>Opex</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {{#pago_os_periodo}}   
                            <tr>
                                <td class="col-md-2">{{id_ot}}
                                    <input type="hidden" id="id_ot" name="id_ot" value="{{id_ot}}"/>
                                </td>
                                <td class="col-md-2">{{tipo_ot}}
                                    <input type="hidden" id="tipo_ot" name="tipo_ot" value="{{tipo_ot}}" />
                                </td>
                                <td>
                                    <input id="monto_total_{{id_ot}}" class="col-sm-5 control-label" value="0">
                                    <input type="hidden" id="monto_total_hiden_{{id_ot}}" name="monto_total" value="0" />
                                </td>
                                <td>
                                    <input type="checkbox" id="check_capex" name="check_capex" onclick="checkPagar()" />
                                    <input type="hidden" id="check_capex_value" name="check_capex_value" value="NO" />
                                </td>
                                <td id="capex">{{capex_ot}}
                                    <input type="hidden" id="capex_ot" name="capex_ot" value="{{capex_ot}}" />
                                </td>
                                <td>
                                    <input type="checkbox" id="check_opex" name="check_opex" onclick="checkPagar()">
                                    <input type="hidden" id="check_opex_value" name="check_opex_value" value="NO" />
                                </td>
                                <td id="opex" >{{opex_ot}} 
                                    <input type="hidden" id="opex_ot" name="opex_ot" value="{{opex_ot}}" />  
                                </td>
                                <td id="tipo_moneda" >{{tipo_moneda}} 
                                    <input type="hidden" id="tipo_moneda" name="tipo_moneda" value="{{tipo_moneda}}" />  
                                </td>
                                <td class="col-md-2">{{fecha_aprobacion_ot}} 
                                    <input type="hidden" id="fecha_aprobacion_ot" name="fecha_aprobacion_ot" value="{{fecha_aprobacion_ot}}" /> 
                                </td>
                            </tr>
                        {{/pago_os_periodo}}   
                    </tbody>
                </table>
            </div>
        </fieldset>
        <div class="col-md-4 text-left">
            <!--button type="button" class="btn btn-default btn-xs" id="download" data-loading-text="Descargando..."data-processing-text="Generando..." data-filters="{{JSON filtros}}" data-periodo="{{periodo}}">Exportar Registros</button-->
        </div>
        <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-default pull-right" id="GuardarCosteo" data-saving-text="Guardando..."data-processing-text="Guardando..." >Guardar Costeo</button>
        </div>
    </div>
</form>
<style>
    .fixed-table-container thead th .th-inner, .fixed-table-container tbody td .th-inner{
        line-height: 15px;
        text-align: center;
    }
</style>
<script src="js/bootstrap-listfilter.js" type="text/javascript" charset="utf-8"></script>
<script src="js/jquery-fileDownload.js" type="text/javascript" charset="utf-8"></script>