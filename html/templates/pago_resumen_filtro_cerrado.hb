<script src="js/bootstrap-table.js" type="text/javascript" charset="utf-8"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
<script src="js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
<table border="0px" width ="100%"  style="/*margin-left: 5px; */margin-top:20px;">
   <td width ="15%" valign="TOP">
            <form class="form-horizontal siom-form-tiny" role="form" action="#/pago/resumen/cerrado/filtro" method="POST" id="pago-resumen-filtro">
               <fieldset class="siom-fieldset" style="padding-bottom:0px; margin-top:0px; margin-left:0px; shape-margin:0px;  ">
                  <legend>Seleccione Filtros</legend>
            <table border="0px" width ="100%" ">
               <tr>
                  <td width="40%">
                     <button type="button" class="btn btn-primary  btn-xs btn-margen" id="Limpiarfiltro">Limpiar</button>
                  </td>
                 
                  <td width="60%">
                     <button type="button"  class="btn btn-primary  btn-xs btn-margen" id="BuscarResumenPago" data-tipo="OS">Buscar</button>
                  </td>
               </tr>
               <tr>
                  <td colspan="2">
                     <hr>
                  </td>
               </tr>
               <tr>
                  <td  align="right">
                   <label for="id_papre_seleccion_contrato " title='Año'  class="control-label" >Año</label>
                  </td>
                  <td class="control-label-th">
                        <input type="text" class="form-control" id="pafi_anio" name="pafi_anio" maxlength="4" data-width="100%" value="{{data.pafi_anio}}" />
                  </td>
               </tr>
               <tr  >
                  <td colspan="2" height="10" >
                  </td>
               </tr>
               <tr >
                  <td  align="right">
                     <label for="pafi_mes" title='mes del contrato solicitado' class="control-label" >Mes</label>
                  </td>
                  <td class="control-label-th">
                     <select class="botones text-ancho" data-width="100%"  name="pafi_mes">
                           <option value="00">TODOS</option>
                           <option value="01">ENERO</option>
                           <option value="02">FEBRERO</option>
                           <option value="03">MARZO</option>
                           <option value="04">ABRIL</option>
                           <option value="05">MAYO</option>
                           <option value="06">JUNIO</option>
                           <option value="07">JULIO</option>
                           <option value="08">AGOSTO</option>
                           <option value="09">SEPTIEMBRE</option>
                           <option value="10">OCTUBRE</option>
                           <option value="11">NOVIEMBRE</option>
                           <option value="12">DICIEMBRE</option>
                        </select>
                  </td>
               </tr>
               <tr>
                  <td  colspan="2" height="10">
                  </td>
               </tr>
            </table>
               </fieldset>
            </form>
         </td>
   <td width ="85%">
      <div  id="pago-resumen-lista-cerrado" style="border: 0px solid #990000;"></div>
         </td>
   </table>
<script src="js/pago_presupuesto.js" type="text/javascript" charset="utf-8"></script>
<script src="js/pago_detalle_filtro.js" type="text/javascript" charset="utf-8"></script>