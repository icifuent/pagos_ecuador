(function($) { 

	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");
	  	$.fileDownload("rest/contrato/"+window.contract+"/pago/os/periodo/"+$btn.data("periodo")+"/descargar",{httpMethod:"POST",data:$.param($btn.data("filters")),
		      prepareCallback:function(url) {
		          $btn.button("processing");
		      },
		      successCallback: function(url) {
		          $btn.button('reset')
		      },
		      failCallback: function(responseHtml, url) {
		          $btn.button('reset')
		          alert(responseHtml);
		      }
	  	});  		
	});

})(jQuery);
