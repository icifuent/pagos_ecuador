
/*Valida numericos */
function validaNumericos(event) {
    if(event.charCode >= 48 && event.charCode <= 57){
    return true;
    }
    return false;        
}   

function parsePesoJs(valor) {

    var value = valor;
    if(null!=value){
        value = value.toString().replace(".", ",");  
        var parts = value.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        value= parts.join("."); 
        return value;       
    }       
}   



/*Funcion invocada cuando se hace click sobre alguno de los checkbox de pagar*/
function checkPagar() {
	UpdateTotal();
	return true;
}

function checkPagarCapex(valueOT) {
	
	console.log("CHECK");
	console.log(valueOT);
	form   = $("#siom-form-costeo");	
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].val() == valueOT) {
			if (opexPagoSel[i].is(':checked')) {
				opexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}

	UpdateTotal();
	/* Session para guardar elementos checked*/
	/*
	var checkedList = [];
	if(window.sessionStorage.getItem('checked')){
		checkedList = window.sessionStorage.getItem('checked');
		console.log("SE AGREGA" + capexPagoSel);
		checkedList.push(capexPagoSel);
		window.sessionStorage.setItem('checked', checkedList);
	}else{
		console.log("SE AGREGA" + capexPagoSel);
		checkedList.push(capexPagoSel);
		window.sessionStorage.setItem('checked', checkedList);
	}
	*/
	return true;
}

function checkPagarOpex(valueOT) {
	
	console.log("CHECK");
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		//alert(opexPagoSel[i].val());
		if (opexPagoSel[i].val() == valueOT) {
			if (capexPagoSel[i].is(':checked')) {
				capexPagoSel[i].prop('checked',false);
			}
			break;
		}
	}
	UpdateTotal();
	return true;
}
/*
	Chequea 
*/
$("#check_capex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_capex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	opexPagoSel = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (opexPagoSel[i].is(':checked')) {
			opexPagoSel[i].prop('checked',false);
			break;
		}
	}
	UpdateTotal();
	return true;
});

$("#check_opex_all").on("click",function(e){
	form   = $("#siom-form-costeo");
	var c = this.checked;
	$("input[name='check_opex']").prop('checked',c);
	
	id_ot  = $(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	capexPagoSel = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	
	for(i=0;i<id_ot.length;i++){
		if (capexPagoSel[i].is(':checked')) {
			capexPagoSel[i].prop('checked',false);
			break;
		}
	}
	UpdateTotal();
	return true;
});

/*Funcion que actualiza el total de costeo tanto para el monto CAPEX como el monto OPEX*/
function UpdateTotal(){
	form   = $("#siom-form-costeo");
	id_ot  = form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	check_opex = form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	capex_ot = form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
	opex_ot =form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
	
	totalCapex=0;
	totalOpex=0;
	totalCabecera=0;
	totCabeceraCapex=0;
	totCabeceraOpex=0;

	for(i=0;i<id_ot.length;i++){
		totalCapex=0;
		totalOpex=0;
		valor = id_ot[i].attr("value");
		if(check_capex[i].is(':checked')){
			totalCapex = totalCapex + parseFloat(capex_ot[i].val());
			totalCabecera=totalCabecera+totalCapex;
			totCabeceraCapex=totCabeceraCapex+totalCapex;
		};

		if(check_opex[i].is(':checked')){
			totalOpex = totalOpex + parseFloat(opex_ot[i].val());
			totalCabecera=totalCabecera+totalOpex;
			totCabeceraOpex=totCabeceraOpex+totalOpex;
		}

		$("#monto_total_"+valor).val(parsePesoJs(totalCapex+totalOpex));


		
	}


	totalCabecera=parsePesoJs(totalCabecera.toFixed(2));
	totCabeceraCapex=parsePesoJs(totCabeceraCapex.toFixed(2));
	totCabeceraOpex=parsePesoJs(totCabeceraOpex.toFixed(2));
	

	$("#monto_total_cabecera").val(totalCabecera);
	$("#monto_total_capex").val(totCabeceraCapex);
	$("#monto_total_opex").val(totCabeceraOpex);
}

/*Mensaje de alerta de sesion */
function AlertaGuardado()
{
	alert("Sesion guardada");
}

/*logica de guardado de sesion */
function saveSession(){

	/*console.log("guardo la sesion ");*/
	form     	= $("#siom-form-costeo");
	id_ot  		= form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
	check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
	check_opex 	= form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
	capex_ot 	= form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
	opex_ot 			= form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
	check_opex_value 	= form.find(':input[name="check_opex_value"]').map(function(){ return $(this); }).get();
	check_capex_value 	= form.find(':input[name="check_capex_value"]').map(function(){ return $(this); }).get();

	if(window.sessionStorage.getItem('str_id_ot')){
		/*console.log("EXISTE Session");*/
		var str_id_ot = window.sessionStorage.getItem('str_id_ot');	
		var str_capex_ot = window.sessionStorage.getItem('str_capex_ot');
		var str_check_capex_value = window.sessionStorage.getItem('str_check_capex_value');
		var str_opex_ot = window.sessionStorage.getItem('str_opex_ot');
		var str_check_opex_value = window.sessionStorage.getItem('str_check_opex_value');

		/* SOLO USAR EN CASO DE QUE SE TENGA QUE ANALIZAR EL ARRAY COMPLETO PARA BUSCAR LAS POSIBLES COINCIDENCIAS
		str_id_ot = str_id_ot.split(";");
		str_capex_ot = str_capex_ot.split(";"); 
		str_check_capex_value = str_check_capex_value.split(";");
		str_opex_ot = str_opex_ot.split(";");
		str_check_opex_value = str_check_opex_value.split(";");

		str_id_ot = str_id_ot.slice(1);
		str_capex_ot = str_capex_ot.slice(1);
		str_check_capex_value = str_check_capex_value.slice(1);
		str_opex_ot = str_opex_ot.slice(1);
		str_check_opex_value = str_check_opex_value.slice(1);
		*/
		string_id="";
		string_capex_valor="";
		string_capex_selec="";
		string_opex_valor="";
		string_opex_selec="";

		for(i=0;i<id_ot.length;i++){
			string_id=string_id + ";" + id_ot[i].val();
			string_capex_valor=string_capex_valor +";"+ capex_ot[i].val();
			string_opex_valor=string_opex_valor +";"+ opex_ot[i].val();
			if(check_opex[i].is(':checked')){
				check_opex_value[i].val("SI");
				string_opex_selec=string_opex_selec + ";SI";
			}else{
				check_opex_value[i].val("NO");
				string_opex_selec=string_opex_selec + ";NO";
			}
			if(check_capex[i].is(':checked')){
				check_capex_value[i].val("SI");
				string_capex_selec=string_capex_selec + ";SI";
			}else{
				check_capex_value[i].val("NO");
				string_capex_selec=string_capex_selec +";NO";
			}
		}

		string_id = str_id_ot.concat(string_id);
		string_capex_valor = str_capex_ot.concat(string_capex_valor);
		string_capex_selec = str_check_capex_value.concat(string_capex_selec);
		string_opex_valor = str_opex_ot.concat(string_opex_valor);
		string_opex_selec = str_check_opex_value.concat(string_opex_selec);

		window.sessionStorage.setItem('str_id_ot', string_id);
		window.sessionStorage.setItem('str_capex_ot', string_capex_valor);
		window.sessionStorage.setItem('str_check_capex_value', string_capex_selec);
		window.sessionStorage.setItem('str_opex_ot', string_opex_valor);
		window.sessionStorage.setItem('str_check_opex_value', string_opex_selec);


		document.getElementById("str_id_ot").value = string_id;
		document.getElementById("str_capex_ot").value = string_capex_valor;
		document.getElementById("str_check_capex_value").value = string_capex_selec;
		document.getElementById("str_opex_ot").value = string_opex_valor;
		document.getElementById("str_check_opex_value").value = string_opex_selec;

	} else {
		
		/*console.log("no guardo la sesion");
		console.log("NO EXISTE SESSION");*/
		

		string_id="";
		string_capex_valor="";
		string_capex_selec="";
		string_opex_valor="";
		string_opex_selec="";

		for(i=0;i<id_ot.length;i++){
			string_id=string_id + ";" + id_ot[i].val();
			string_capex_valor=string_capex_valor +";"+ capex_ot[i].val();
			string_opex_valor=string_opex_valor +";"+ opex_ot[i].val();
			if(check_opex[i].is(':checked')){
				check_opex_value[i].val("SI");
				string_opex_selec=string_opex_selec + ";SI";
			}else{
				check_opex_value[i].val("NO");
				string_opex_selec=string_opex_selec + ";NO";
			}
			if(check_capex[i].is(':checked')){
				check_capex_value[i].val("SI");
				string_capex_selec=string_capex_selec + ";SI";
			}else{
				check_capex_value[i].val("NO");
				string_capex_selec=string_capex_selec +";NO";
			}
		}

		window.sessionStorage.setItem('str_id_ot', string_id);
		window.sessionStorage.setItem('str_capex_ot', string_capex_valor);
		window.sessionStorage.setItem('str_check_capex_value', string_capex_selec);
		window.sessionStorage.setItem('str_opex_ot', string_opex_valor);
		window.sessionStorage.setItem('str_check_opex_value', string_opex_selec);

		document.getElementById("str_id_ot").value = string_id;
		document.getElementById("str_capex_ot").value = string_capex_valor;
		document.getElementById("str_check_capex_value").value = string_capex_selec;
		document.getElementById("str_opex_ot").value = string_opex_valor;
		document.getElementById("str_check_opex_value").value = string_opex_selec;
	}
}

function loadSession(){


	console.log("cargo la sesion");
	var str_id_ot = window.sessionStorage.getItem('str_id_ot');
	var str_capex_ot = window.sessionStorage.getItem('str_capex_ot');
	var str_check_capex_value = window.sessionStorage.getItem('str_check_capex_value');
	var str_opex_ot = window.sessionStorage.getItem('str_opex_ot');
	var str_check_opex_value = window.sessionStorage.getItem('str_check_opex_value');

	console.log(str_id_ot);
	console.log(str_capex_ot);
	console.log(str_check_capex_value);
	console.log(str_opex_ot);
	console.log(str_check_opex_value);

	document.getElementById("str_id_ot").value = string_id;
	document.getElementById("str_capex_ot").value = string_capex_valor;
	document.getElementById("str_check_capex_value").value = string_capex_selec;
	document.getElementById("str_opex_ot").value = string_opex_valor;
	document.getElementById("str_check_opex_value").value = string_opex_selec;
}

(function($) { 

	$("button#download").on("click",function(e){
		e.stopImmediatePropagation();
	  	$btn = $(this);
	  	$btn.button("loading");	
	  	$.fileDownload("rest/pago/descargar/detalle/costeo/contrato/"+window.contract+"/periodo/"+$btn.data("periodo")+"/lista",{httpMethod:"POST",data:$.param($btn.data("filters")),
		    prepareCallback:function(url) {
		        $btn.button("processing");
		    },
		    successCallback: function(url) {
		        $btn.button('reset')
		    },
		    failCallback: function(responseHtml, url) {
		        $btn.button('reset')
		    }
	  	});		
	});


	

	$("#GuardarCosteo").click(function(){
	
		button   	= $(this);
		form     	= $("#siom-form-costeo");
		id_ot  		= form.find(':input[name="id_ot"]').map(function(){ return $(this); }).get();
		check_capex = form.find(':input[name="check_capex"]').map(function(){ return $(this); }).get();
		check_opex 	= form.find(':input[name="check_opex"]').map(function(){ return $(this); }).get();
		capex_ot 	= form.find(':input[name="capex_ot"]').map(function(){ return $(this); }).get();
		opex_ot 			= form.find(':input[name="opex_ot"]').map(function(){ return $(this); }).get();
		check_opex_value 	= form.find(':input[name="check_opex_value"]').map(function(){ return $(this); }).get();
		check_capex_value 	= form.find(':input[name="check_capex_value"]').map(function(){ return $(this); }).get();
		periodo 			= document.getElementById("periodo").value;
		actividad 			= document.getElementById("actividad").value;

	
		//monto_total_cabecera = $("#monto_total_cabecera").val();
		monto_total_cabecera =document.getElementById("monto_total_cabecera").value;
		if (0==monto_total_cabecera) {
			alert("Debe seleccionar al menos una orden de trabajo, para generar costeo."); 
			return true;
		}

		monto_total_capex =document.getElementById("monto_total_capex").value;
		lineapresupuestariacapex =document.getElementById("lineacapex").value;
		if ("0,00"==lineapresupuestariacapex && 0 < monto_total_capex) {
			alert("Debe seleccionar al menos una linea de presupuesto capex, para generar costeo."); 
			return true;
		}

		monto_total_opex =document.getElementById("monto_total_opex").value;
		lineapresupuestariaopex =document.getElementById("lineaopex").value;
		if ("0,00"==lineapresupuestariaopex && 0 < monto_total_opex) {
			/*console.log("paso por validador opex");*/
			alert("Debe seleccionar al menos una linea de presupuesto opex, para registrar costeo."); 
			return true;
		}

		monto_total_opex =document.getElementById("monto_total_opex").value;
		lineapresupuestariaopex =document.getElementById("lineaopex").value;
		if ("0,00"<lineapresupuestariaopex && 0 == monto_total_opex) {
		
			alert("Debe seleccionar al menos una orden de trabajo tipo Opex, para registrar costeo."); 
			return true;
		}

		monto_total_capex =document.getElementById("monto_total_capex").value;
		lineapresupuestariacapex =document.getElementById("lineacapex").value;
		if ("0,00"<lineapresupuestariacapex && 0 == monto_total_capex) {
			/*console.log("paso por validador capex");*/
			alert("Debe seleccionar al menos una orden de trabajo tipo Capex, para registrar costeo."); 
			return true;
		}

		console.log("paso por validador opex cantidad"+monto_total_opex);
		console.log("paso por validador capex cantidad"+monto_total_capex);
		/*
		string_id="";
		string_capex_valor="";
		string_capex_selec="";
		string_opex_valor="";
		string_opex_selec="";
		
		for(i=0;i<id_ot.length;i++){
			string_id=string_id + ";" + id_ot[i].val();
			string_capex_valor=string_capex_valor +";"+ capex_ot[i].val();
			string_opex_valor=string_opex_valor +";"+ opex_ot[i].val();
			
			if(check_opex[i].is(':checked')){
				check_opex_value[i].val("SI");
				string_opex_selec=string_opex_selec + ";SI";
			}else{
				check_opex_value[i].val("NO");
				string_opex_selec=string_opex_selec + ";NO";
			}
			if(check_capex[i].is(':checked')){
				check_capex_value[i].val("SI");
				string_capex_selec=string_capex_selec + ";SI";
			}else{
				check_capex_value[i].val("NO");
				string_capex_selec=string_capex_selec +";NO";
			}
		}
		*/
		saveSession();
		/*
		document.getElementById("str_id_ot").value = string_id;
		document.getElementById("str_capex_ot").value = string_capex_valor;
		document.getElementById("str_check_capex_value").value = string_capex_selec;
		document.getElementById("str_opex_ot").value = string_opex_valor;
		document.getElementById("str_check_opex_value").value = string_opex_selec;
		*/

		form     	= $("#siom-form-costeo");
		confirm("El costeo será guardado <b></b><br>Desea continuar?",function(status){
			if(status){
				console.log("paso por el cerrar ");
				button.button("Guardando...");
				sessionStorage.clear();
				form.submit();
				//return true;
			}
		});
	});
	
})(jQuery);