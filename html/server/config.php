<?php
$ENV = "prod"; 

$EMAIL_FROM = array('siom.notificacion@movistar.cl'=>'SIOM');

include_once("config_perfil.php");

include_once("config-".$ENV.".php");

/*
* FUNCION PARA GENERAR UNA VERSION A LOS ARCHIVOS JS, CSS
*/
function auto_version($file) {
	/*
    $file = str_replace('http://', '', $file);
    $file = str_replace('https://', '', $file);
    $file = str_replace($_SERVER['SERVER_NAME'], '', $file);
    $full_file = $_SERVER['DOCUMENT_ROOT'] . $file;
    if (strpos($file, '/') !== 0 || !file_exists($full_file)) {
        $full_file = substr($_SERVER['SCRIPT_FILENAME'], 0, -strlen($_SERVER['SCRIPT_NAME']));
        $full_file .= $file;
        if (!file_exists($full_file)) {
            return $file;
        }
    }
    */
    #RECUPERA LA FECHA DE LA ULTIMA MODIFICACION DEL ARCHIVO
    $mtime = filemtime($file);
    #AGREGA LA FECHA DE LA ULTIMA MODIFICACION AL ARCHIVO
    $new_file = "$file" ."?v=" .$mtime;
    return $new_file;
    }
?>
